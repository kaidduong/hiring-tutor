export const environment = {
  production : true,
  apiBaseURI : 'https://localhost:44321/api/',
  baseResourceURI : 'https://localhost:44321/resources',
  urlImage:'https://localhost:44321/',

};
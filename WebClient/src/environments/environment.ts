// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production : false,

  // Debug 
   //apiBaseURI : 'https://localhost:44321/api/',
   //baseResourceURI : 'https://localhost:44321/resources',
   //urlImage : 'https://localhost:44321/',


  // Test
   apiBaseURI : 'http://hiringtutors.azurewebsites.net/api/',
   baseResourceURI : 'http://hiringtutors.azurewebsites.net/resources',

   urlImage : 'http://hiringtutors.azurewebsites.net/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
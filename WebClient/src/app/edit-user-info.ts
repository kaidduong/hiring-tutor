export class EditUserInfo {
     userInfoView :UserInfoView ;
     hasUpdateInfoUser : boolean;
     addressView :  UpdateAdddressView;
     hasUpdateAddress:boolean;
}

export class UserInfoView{
    fullName: string;
    phone:string;
}

export class UpdateAdddressView{
    id: number;
    typeUpdate: number;
    street: string;
    commune : string;
    houseNumber : number;
    district : string;
    city : string;
}
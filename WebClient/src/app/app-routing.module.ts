import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { AuthGuard } from './auth/check-login.guard';
import { UserLoginComponent } from './user/user-login/user-login.component';
import {RegisterComponent} from './user/register/register.component';

const routes: Routes = [
  { path:'login', component: UserLoginComponent,canActivate: [AuthGuard]},//canactive
  { path:'register', component: RegisterComponent},
  { path:'', component: AboutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatSortModule, MatPaginatorModule } from '@angular/material';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ModalModule } from 'ngx-bootstrap/modal';

import { AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';

import { SliderModule } from 'angular-image-slider';

import { NgxPaginationModule } from 'ngx-pagination';

import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AboutComponent } from './about/about.component';
import { BodyRoutingModule } from './body/body-routing.module';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { ConfirmDialogComponent } from './body/dialog/dialog.component';
import { HomeComponent } from './body/home/home.component';
import { LeftBarComponent } from './body/left-bar/left-bar.component';
import { NewsManagementComponent } from './body/news-management/news-management.component';
import { SignupTutorComponent } from './body/signup-tutor/signup-tutor.component';
import {TutorManagementComponent} from './body/tutor-management/tutor-management.component';
import { TutorComponent } from './body/tutor/tutor.component';
import { UserManagementComponent } from './body/user-management/user-management.component';
import { WrittingComponent } from './body/writting/writting.component';
import { AuthInterceptor } from './shared/interceptor/auth-interceptor';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { PersonalComponent } from './user/personal/personal.component';
import { RegisterComponent } from './user/register/register.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { TutorDetailComponent } from './body/tutor-management/tutor-detail/tutor-detail.component';
import { UserDetailComponent } from './body/user-management/user-detail/user-detail.component';
import { WrittingDetailComponent } from './body/writting/writting-detail/writting-detail.component';
const MaterialsComponents = [
  MatButtonModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatPaginatorModule

];

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("659589669275-bkeko1ise012qmsrbl5h6663s99lf9es.apps.googleusercontent.com")
      }
    ]
  );
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    NavComponent,
    FooterComponent,
    UserLoginComponent,
    ConfirmDialogComponent,
    HomeComponent,
    AboutComponent,
    DropdownDirective,
    RegisterComponent,
    SignupTutorComponent,
    WrittingComponent,
    WrittingDetailComponent,

    UserManagementComponent,
    PersonalComponent,
    UserDetailComponent,
    TutorComponent,
    EditUserComponent,
    TutorManagementComponent,
    TutorDetailComponent,
    LeftBarComponent,
    NewsManagementComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BodyRoutingModule,
    BrowserAnimationsModule,
    SliderModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    NgbModule,
    MaterialsComponents,
    NgxPaginationModule,
    ModalModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    }),
    
  ],
  exports: [MaterialsComponents],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs,
  },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
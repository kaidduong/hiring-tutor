import { Directive, HostListener, HostBinding } from '@angular/core';
import { ElementRef } from '@angular/core';
@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective {
    constructor(private elementRef:ElementRef){}
    @HostBinding('class.open') isOpen = false;
    @HostListener('click') toggleOpen() {
        this.isOpen = !this.isOpen;
    }
    @HostListener('mouseleave') toggleOpen2() {
        this.isOpen = !this.isOpen;
    }
    
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import * as _ from 'underscore';

import { environment } from 'src/environments/environment';
import { EditUserInfo } from 'src/app/edit-user-info';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  isRole: boolean = false;

  constructor(private router: Router, private http: HttpClient) { }

  // hàm gửi Token google user api đến server sử lý
  googleLogin(idToken: string) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    var payload = { token: idToken };
    return this.http.post(environment.apiBaseURI + 'auth/login', JSON.stringify(payload), { headers }).toPromise();
  }

  login(data: any) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(environment.apiBaseURI + 'Auth/login-user', data, { headers: headers, observe: 'response', responseType: 'json' });

  }

  register(data: any) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(environment.apiBaseURI + 'Auth/register', data, { headers: headers, observe: 'response', responseType: 'json' });
  }
  changePassword(data: any) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put(environment.apiBaseURI + 'User/ChangePassword', data, { headers: headers, observe: 'response', responseType: 'json' });
  }
  editUser(data: EditUserInfo) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put(environment.apiBaseURI + 'User/EditUserInfor', data, { headers: headers, observe: 'response', responseType: 'json' });
  }

  getAllUserList() {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'User/GetAllUsers', { headers: headers, observe: 'response', responseType: 'json' });
  }

  tutorPostFile(fileToUpload: File) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    var formData: FormData = new FormData();
    formData.append('File', fileToUpload);
    formData.append('Path', 'TutorCertificate');
    return this.http.post(environment.apiBaseURI + 'Image/uploadimage', formData);
  }

  getDetailUserInfo(id: number) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'User/GetUserInforById?userId=' + id, { headers: headers, observe: 'response', responseType: 'json' });
  }


  getGetUserInfo() {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'User/GetUserInfo', { headers: headers, observe: 'response', responseType: 'json' });
  }
  postFile(fileToUpload: File) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    var formData: FormData = new FormData();
    formData.append('File', fileToUpload);
    formData.append('Path', 'UserAvatar');
    return this.http.post(environment.apiBaseURI + 'Image/uploadimage', formData);
  }
  removeUser(id) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiBaseURI + 'User/RemoveUser/' + id, {}, { headers: headers, observe: 'response', responseType: 'json' });

  }
}
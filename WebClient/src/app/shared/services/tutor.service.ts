import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TutorService {

  constructor(private http: HttpClient) { }
  getAllTutorList() {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'Tutor/GetAll', { headers: headers, observe: 'response', responseType: 'json' });
  }
  deleteTutor(id) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiBaseURI + 'Tutor/Delete?id=' + id, {}, { headers: headers, observe: 'response', responseType: 'json' });
  }
  getDetailTutorById(id: number) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'Tutor/GetById/' + id, { headers: headers, observe: 'response', responseType: 'json' });
  }

  getDetailTutor() {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'Tutor',{ headers: headers, observe: 'response', responseType: 'json' });
  }
  confirm(data){
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put(environment.apiBaseURI + 'Tutor/ConfirmTutor',data,{ headers: headers, observe: 'response', responseType: 'json' });

  }

}

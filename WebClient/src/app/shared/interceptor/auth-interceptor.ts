import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { BsModalService } from 'ngx-bootstrap/modal';

import { ConfirmDialogComponent } from 'src/app/body/dialog/dialog.component';

import { MessageService } from '../services/message.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private modalService: BsModalService, private router: Router, private messageService: MessageService) {

  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let accessToken = localStorage.getItem("auth_token");
    if(accessToken)
    {
        request = request.clone({
        setHeaders: {
            Authorization: `Bearer ${accessToken}`
        }
        });
    }

    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
          // auto logout if 401 response returned from api
          localStorage.removeItem('auth_token');
          localStorage.removeItem("currentUser");
          this.messageService.sendMessage({reloadUser: true});
          const initialState = {
            title: 'Thông báo',
            content: 'Token đã hết hạn, vui lòng đăng nhập lại!',
            btnTittle: 'Đồng ý'
          };
          this.modalService.show(ConfirmDialogComponent, {initialState}).content.okCallBack.subscribe(event => {
            this.modalService.hide(1);
            this.router.navigate(['/signin']);
           });
      }
      return throwError(err);
    }));
  }
}
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { MustMatch } from 'src/app/shared/must-match.validator';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { from } from 'rxjs';

import { UserService } from 'src/app/shared/services/user.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-signup-tutor',
  templateUrl: './signup-tutor.component.html',
  styleUrls: ['./signup-tutor.component.css']
})
export class SignupTutorComponent implements OnInit {
  modalRef: BsModalRef;
  registerForm: FormGroup;
  submitted = false;
  closeResult: string;
  completed = false;
  fileToUpload1: File = null;
  imageUrl1 = environment.urlImage + 'Resources/Images/UserAvatarDefault/User-Role-Permission.png';
  dateofWeak = ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'thứ 6', 'thứ 7', 'Chủ nhật'];

  constructor(private http: HttpClient, private formBuilder: FormBuilder, private modalService: NgbModal, private modal: BsModalService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      describe: ['', Validators.required],
      title: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern("[0-9 ]{12}")]],
      firstName: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    //upload file

  }
  get f() { return this.registerForm.controls; }
  onSubmit() {


    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));


  }
  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modal.show(template);
  }


  handleFileInput1(file: FileList) {
    this.fileToUpload1 = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl1 = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload1);
  }

  OnSubmit(Image) {
    this.userService.tutorPostFile(this.fileToUpload1).subscribe(
      data => {
        console.log('done');
        Image.value = null;
      }
    );
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxPrintModule } from 'ngx-print';

import { CheckRoleUser } from '../auth/check-role-user';
import { EditUserComponent } from '../user/edit-user/edit-user.component';
import { PersonalComponent } from '../user/personal/personal.component';

import { BodyComponent } from './body.component';

import { HomeComponent } from './home/home.component';
import { NewsManagementComponent } from './news-management/news-management.component';
import { SignupTutorComponent } from './signup-tutor/signup-tutor.component';
import { TutorManagementComponent } from './tutor-management/tutor-management.component';
import { TutorComponent } from './tutor/tutor.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { WrittingComponent } from './writting/writting.component';
import { TutorDetailComponent } from './tutor-management/tutor-detail/tutor-detail.component';
import { UserDetailComponent } from './user-management/user-detail/user-detail.component';
import { WrittingDetailComponent } from './writting/writting-detail/writting-detail.component';

const routes: Routes = [
  {
    path: '', component: BodyComponent, canActivate: [CheckRoleUser], children: [
      { path: 'home', component: HomeComponent },
      { path: 'signup-tutor', component: SignupTutorComponent },
      { path: 'writting', component: WrittingComponent },
      { path: 'writting-detail/:id', component: WrittingDetailComponent },
      { path: 'list-user', component: UserManagementComponent },
      { path: 'personal', component: PersonalComponent },
      { path: 'user-detail/:id', component: UserDetailComponent },
      { path: 'tutor', component: TutorComponent },
      { path: 'edit-user', component: EditUserComponent },
      { path: 'list-tutor', component: TutorManagementComponent },
      {path:'tutor-detail/:id',component:TutorDetailComponent},
      {path:'list-news',component:NewsManagementComponent}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes), NgxPrintModule],
  exports: [RouterModule]
})

export class BodyRoutingModule { }

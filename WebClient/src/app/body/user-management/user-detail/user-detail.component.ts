import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from 'src/app/shared/services/user.service';

import { Subscription } from 'rxjs';
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  id: number;
  public subscription: Subscription;
  public user = null;
  public noData = 'Không có dữ liệu';
  public address = '';
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private userService: UserService) {
    this.id = this.activatedRoute.snapshot.params.id;
  }


  ngOnInit() {
    // this.subscription = this.activatedRoute.params.subscribe(params => {
    //   this.id = params['id'];
    // });
    this.userService.getDetailUserInfo(this.id).subscribe((data) => {
      this.user = data.body;
      console.log(this.user);
      console.log(this.user.userName);
      if (this.user.addressView) {
        if (this.user.addressView.houseNumber ) {
          this.address +="Số nhà "+ this.user.addressView.houseNumber;
        }
        if (this.user.addressView.street) {
          this.address += ", đường " + this.user.addressView.street;
        }
        if (this.user.addressView.district) {
          this.address += ", quận " + this.user.addressView.district;
        }
        if (this.user.addressView.city) {
          this.address += ", thành phố " + this.user.addressView.city;
        }
      }
      console.log(this.address);
    });
  }
  gotoUserpage() {
    this.router.navigate(['/list-user']);
  }
}

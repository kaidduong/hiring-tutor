import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  userList;
  public popoverTitle: string = 'Xác nhận';
  public popoverMessage: string = 'Bạn có muốn xóa không';
  public cancelClicked: boolean = false;
  public urlAvatar = environment.urlImage;
  notification = null;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private http: HttpClient, private userService: UserService) { }

  ngOnInit() {
    this.GetAllUserData();
  }
  GetAllUserData() {
    this.userService.getAllUserList().subscribe((data: any) => {
      this.userList = data.body;
      this.dataSource = new MatTableDataSource(this.userList);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(data);
    });
  }
  displayedColumns: string[] = ['order', 'userName', 'fullname', 'email', 'avatar', 'phoneNumber', 'function'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  removeUser(id) {

    this.userService.removeUser(id).subscribe(response => {
      if (response) {
        this.showNotification();
        this.GetAllUserData();
      }
    });

  }
  showNotification() {
    this.notification = { class: 'text-success', message: 'Xóa thành công' }
    setTimeout(() => {
      this.notification = null;
    }, 4000);
  }
}

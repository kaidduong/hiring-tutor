import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { TutorService } from 'src/app/shared/services/tutor.service';

@Component({
  selector: 'app-tutor',
  templateUrl: './tutor.component.html',
  styleUrls: ['./tutor.component.css']
})
export class TutorComponent implements OnInit {

  modalRef: BsModalRef;

  closeResult: string;
  config: any;
  tutorList;
  selectTutor;
  addressTutor='';


  constructor(private modalService: NgbModal, private modal: BsModalService, private tutorService: TutorService) {

    this.config = {
      itemsPerPage: 3,
      currentPage: 1,
      // totalItems: this.tutorList.length
    };
  }

  ngOnInit() {

    this.getAllTutor();

  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public openModal(template: TemplateRef<any>, tutor) {
    this.modalRef = this.modal.show(template);
    this.selectTutor = {
      // tslint:disable-next-line:max-line-length
      name: tutor.name, email: tutor.email, job: tutor.job, teachingType: tutor.teachingType, tuitionPerHour: tutor.tuitionPerHour, introduction: tutor.introduction, schedule: tutor.schedule, address: tutor.address,
      subjects: tutor.subjects
    };
    if (tutor.address) {
      this.addressTutor='';
      if (tutor.address.houseNumber) {
        this.addressTutor += 'Số nhà ' + tutor.address.houseNumber;
      }
      if (tutor.address.street) {
        this.addressTutor += ', đường ' + tutor.address.street;
      }
      if (tutor.address.district) {
        this.addressTutor += ', quận ' + tutor.address.district;
      }
      if (tutor.address.city) {
        this.addressTutor += ', thành phố ' + tutor.address.city;
      }
    }
    
    

  }
  public close() {
    this.modalRef.hide();
  }
  getAllTutor() {
    this.tutorService.getAllTutorList().subscribe((data: any) => {
      this.tutorList = data.body;
      console.log(this.tutorList.length);

    });
  }
  pageChanged(event) {
    this.config.currentPage = event;
  }


}


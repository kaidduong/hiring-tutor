import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { TutorService } from 'src/app/shared/services/tutor.service';

@Component({
  selector: 'app-tutor-detail',
  templateUrl: './tutor-detail.component.html',
  styleUrls: ['./tutor-detail.component.css']
})
export class TutorDetailComponent implements OnInit {
  id;
  tutor;
  dateofWeak = ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ nhật'];
  day = [0, 0, 0, 0, 0, 0, 0];
  address = '';
  confirmdata;
  disabledata;
  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, private tutorService: TutorService) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.getDetailTutorById(this.id);
  }
  getDetailTutorById(id) {
    this.tutorService.getDetailTutorById(this.id).subscribe((data) => {
      this.tutor = data.body;
      console.log(this.tutor.status);

      this.day[0] = this.tutor.scheduleView.monday;
      this.day[1] = this.tutor.scheduleView.tuesday;
      this.day[2] = this.tutor.scheduleView.wednesday;
      this.day[3] = this.tutor.scheduleView.thursday;
      this.day[4] = this.tutor.scheduleView.friday;
      this.day[5] = this.tutor.scheduleView.saturday;
      this.day[6] = this.tutor.scheduleView.sunday;

      if (this.tutor.address) {
        if (this.tutor.address.houseNumber) {
          this.address += 'Số nhà ' + this.tutor.address.houseNumber;
        }
        if (this.tutor.address.street) {
          this.address += ', đường ' + this.tutor.address.street;
        }
        if (this.tutor.address.commune) {
          this.address += ', đường ' + this.tutor.address.commune;
        }
        if (this.tutor.address.district) {
          this.address += ', quận ' + this.tutor.address.district;
        }
        if (this.tutor.address.city) {
          this.address += ', thành phố ' + this.tutor.address.city;
        }
      } else {
        this.address = 'không có dữ liệu';
      }
      console.log(this.address);

    });

  }
  gotoTutorpage() {
    this.router.navigate(['/list-tutor']);
  }
  confirm(id) {
    this.confirmdata = {
      "id": this.id,
      "confirmedType": 4
    };

    this.tutorService.confirm(this.confirmdata).subscribe(data => {
      this.router.navigate(['/list-tutor']);
    });
  }

  disable(id){
    this.disabledata = {
      "id": this.id,
      "confirmedType": 5
    };
    this.tutorService.confirm(this.disabledata).subscribe(data => {
      this.router.navigate(['/list-tutor']);
    });

  }
}

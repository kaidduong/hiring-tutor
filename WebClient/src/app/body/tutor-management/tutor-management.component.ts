import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { HttpClient } from '@angular/common/http';

import { TutorService } from 'src/app/shared/services/tutor.service';

@Component({
  selector: 'app-tutor-management',
  templateUrl: './tutor-management.component.html',
  styleUrls: ['./tutor-management.component.css']
})
export class TutorManagementComponent implements OnInit {

  tutorList;
  notification=null;
  address;
  message;
  public popoverTitle: string = 'Xác nhận';
  public popoverMessage: string = 'Bạn có muốn xóa không?';
  public cancelClicked: boolean = false;
  
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private http: HttpClient, private tutorService: TutorService) { }

  ngOnInit() {
    this.GetAllTutorData();
  }
  GetAllTutorData() {
    this.tutorService.getAllTutorList().subscribe((data: any) => {
      this.tutorList = data.body;
      console.log(this.tutorList.length);
      this.dataSource = new MatTableDataSource(this.tutorList);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    
  }
  displayedColumns: string[] = ['order', 'name', 'phone', 'job', 'address', 'status', 'function'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteTutor(id){

    this.tutorService.deleteTutor(id).subscribe(response => {
      if (response) {
        this.showNotification();
        this.GetAllTutorData();
      }
    });

  }
  showNotification() {
    this.notification = { class: 'text-success', message: 'Xóa gia sư thành công' }
    setTimeout(() => {
      this.notification = null;
    }, 4000);
  }
}


import { Component, OnInit, TemplateRef } from '@angular/core';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import {BsModalService,BsModalRef} from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-writting',
  templateUrl: './writting.component.html',
  styleUrls: ['./writting.component.css']
})
export class WrittingComponent implements OnInit {
  modalRef:BsModalRef;
  closeResult: string;
  constructor(private modalService: NgbModal,private modal:BsModalService) { }
  news = [
    {
      "id": "202083ed-36e8-4a87-bb88-11952d2a9a00",
      "title": "Tìm gia sư tiếng anh lớp 6 lên 7 ",
      "address": "Liên chiễu - Đà Nẵng",
      "subject": "Anh văn",
      "grade": "8",
      "content": "Học viên nam đang học lớp 6 học lực Tiếng Anh kém, tiếp thu hơi chậm nên cần gia sư NỮ NGHIÊM KHẮC và có KHẢ NĂNG TRUYỀN ĐẠT kiến thức dễ hiểu, dễ nhớ. Đề nghị Gia sư giao bài..",
      "tuitionPerHour": "100",
      "addmissionFee": "300",
      "status": "false",
      "time":"3"
    },
    {
      "id": "4cc2c90a-acaf-4c38-a34c-9b936ef0dc86",
      "title": "Tìm gia sư tiếng anh lớp 6  ",
      "address": "Hồ Tây",
      "subject": "Toán",
      "grade": "9",
      "content": "Học viên nam đang học lớp 6 học lực Tiếng Anh kém, tiếp thu hơi chậm nên cần gia sư NỮ NGHIÊM KHẮC và có KHẢ NĂNG TRUYỀN ĐẠT kiến thức dễ hiểu, dễ nhớ. Đề nghị Gia sư giao bài..",
      "tuitionPerHour": "100",
      "addmissionFee": "300",
      "status": "false",
      "time":"3"
    },
    {
      "id": "4ce36c40-5e7b-4ed1-ab54-72188daf572f",
      "title": "Tìm gia sư tiếng anh lớp 6  ",
      "address": "Hồ Tây",
      "subject": "Toán",
      "grade": "9",
      "content": "Học viên nam đang học lớp 6 học lực Tiếng Anh kém, tiếp thu hơi chậm nên cần gia sư NỮ NGHIÊM KHẮC và có KHẢ NĂNG TRUYỀN ĐẠT kiến thức dễ hiểu, dễ nhớ. Đề nghị Gia sư giao bài..",
      "tuitionPerHour": "100",
      "addmissionFee": "300",
      "status": "false",
      "time":"2"
    },

  ]
  ngOnInit() {
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  
  public openModal(template:TemplateRef<any>){
    this.modalRef=this.modal.show(template);
  
  }


}

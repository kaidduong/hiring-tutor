import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrittingDetailComponent } from './writting-detail.component';

describe('WrittingDetailComponent', () => {
  let component: WrittingDetailComponent;
  let fixture: ComponentFixture<WrittingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrittingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrittingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CheckRoleUser implements CanActivate{
  
  constructor(private router: Router){

  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
     if(localStorage.getItem('auth_token') == null){
       this.router.navigate(['']);
     }
     let currentUser = localStorage.getItem('currentUser')
     if(localStorage.getItem('currentUser') != null){
        return JSON.parse(currentUser).roles.length > 0
     }
    return true;
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { UserService } from '../shared/services/user.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  user;
  navforgiasu;
  baidang;
  isadmin = false;
  isgs = false;
  j;


  public urlAvatar = environment.urlImage;
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.j = 0;
    for (let i = 0; i < this.user.roles.length; i++) {
      console.log(this.user.roles[i]);
      if (this.user.roles[i] === 'Tutor new'||this.user.roles[i] === 'Tutor') {
        this.j = 1;
      }
    }
    console.log(this.j);

    if (this.user.roles.find(x => x === 'Admin') === undefined) {
      this.isadmin = false;
    }
    else {
      this.isadmin = true;
    }

  }


  Logout() {
    localStorage.removeItem("auth_token");
    localStorage.removeItem("currentUser");
    this.router.navigateByUrl('/');
  }

}

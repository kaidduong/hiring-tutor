import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from 'src/app/shared/services/user.service';
import { EditUserInfo } from 'src/app/edit-user-info';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  changePassswordForm: FormGroup;
  editUserForm:FormGroup;
  editAddressForm:FormGroup;
  submitted = false;
  notificationSuccess:any;
  notificationError:any;
  notificationUser: any;
  notificationAddress: any;
  loading = false;
  public user = null;
  addressId = -1;
  editUser: EditUserInfo;
  constructor(
    private formBuilder: FormBuilder,
            private  userService: UserService) { }

  ngOnInit() {
    this.editUserForm = this.formBuilder.group({
    fullName: ['', Validators.required],
    phone: ['',[Validators.required,  Validators.pattern("^[0-9]*$"),
    Validators.minLength(10), Validators.maxLength(10)]]
  });
  this.editAddressForm= this.formBuilder.group({
    houseNumber: [''],
    street: ['', Validators.required],
    commune:['', Validators.required],
    district: ['', Validators.required],
    city: ['', Validators.required]
  });
  this.userService.getGetUserInfo().subscribe((data) => {
    this.user = data.body;
    if(this.user.fullName){
      this.editUserForm.controls.fullName.setValue(this.user.fullName);
    }
    if(this.user.phoneNumber){
      this.editUserForm.controls.phone.setValue(this.user.phoneNumber);
    }
    
    if(this.user.addressView){
      this.addressId= this.user.addressView.id;
if(this.user.addressView.houseNumber){
  this.editAddressForm.controls.houseNumber.setValue(this.user.addressView.houseNumber);
}
if(this.user.addressView.street){
  this.editAddressForm.controls.street.setValue(this.user.addressView.street);
}

if(this.user.addressView.commune){
  this.editAddressForm.controls.commune.setValue(this.user.addressView.commune);
}
if(this.user.addressView.district){
  this.editAddressForm.controls.district.setValue(this.user.addressView.district);
}
if(this.user.addressView.city){
  this.editAddressForm.controls.city.setValue(this.user.addressView.city);
}
    }
});

  this.changePassswordForm = this.formBuilder.group({
    oldPassword: ['', Validators.required],
    newPassword: ['', [Validators.required,Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),Validators.minLength(8)]],
    confirmPassword: ['', Validators.required]
}, {
    validator: MustMatch('newPassword', 'confirmPassword')
    });

  }
  get z(){
    return this.editAddressForm.controls;
  }
  get y() {
    return this.editUserForm.controls; 
  }
get f() {return this.changePassswordForm.controls; }
onSubmitUser(){//USER

            this.submitted = true;
            this.hideNotification();
            // stop here if form is invalid
            if (this.editUserForm.invalid) {
                return;
            }
          this.editUser = {userInfoView: { fullName: this.editUserForm.get('fullName').value,
          phone: this.editUserForm.get('phone').value},
          hasUpdateInfoUser: true,
          hasUpdateAddress: false,
          addressView :  null
          
        };
          this.userService.editUser(this.editUser ).subscribe((res : any)=>{
           
            this.showNotification('user');
    
              },(err:any)=>{
                this.loading = false;
              }); 
}

onSubmitAddress(){// ADĐRESS
  this.submitted = true;
  this.hideNotification();
  // stop here if form is invalid
  if (this.editAddressForm.invalid) {
      return;
  }
  this.editUser = {addressView: {
  id: this.addressId,
  typeUpdate:1,
  houseNumber: this.editAddressForm.get('houseNumber').value,
  street: this.editAddressForm.get('street').value,
  commune: this.editAddressForm.get('commune').value,
  district: this.editAddressForm.get('district').value,
  city: this.editAddressForm.get('city').value},
 hasUpdateInfoUser: false,
 hasUpdateAddress: true,
 userInfoView :  null

};
this.userService.editUser(this.editUser ).subscribe((res : any)=>{
 
  this.showNotification('address');

    },(err:any)=>{
      this.loading = false;
    }); 
}

    onSubmitPasssword() {
      
        this.submitted = true;
        this.hideNotification();
        // stop here if form is invalid
        if (this.changePassswordForm.invalid) {
            return;
        }
        if (this.changePassswordForm.get('oldPassword').value == this.changePassswordForm.get('newPassword').value){
          this.notificationError= {  message: 'Mật khẩu mới không được trùng với mật khẩu cũ!' };
          return;
        }

      this.userService.changePassword( {oldPassword:this.changePassswordForm.get('oldPassword').value ,
    newPassword: this.changePassswordForm.get('newPassword').value  }).subscribe((res : any)=>{
           // console.log(res)
           this.notificationError= {  message: '' };
         this.showNotification('success');

           // this.router.navigateByUrl('/login');
          },(err:any)=>{
            this.loading = false;
            this.showNotification('error');
          }); 
    }
    showNotification(type: string){
      switch (type) {
        case 'success':
          this.notificationSuccess= {  message: 'Đổi mật khẩu không thành công!' };
          break;
          case 'error':
            this.notificationError= {  message: 'Đổi mật khẩu không thành công! Mật khẩu cũ không đúng.' };
            break;
            case 'user':
            this.notificationUser= {  message: 'Đổi thông tin người dùng thành công!' };
          break;
          case 'address':
          this.notificationAddress= {  message: 'Đổi địa chỉ liên hệ thành công!' };
          break;
        default:
          break;
      }
  
 }
hideNotification(){
  this.notificationAddress= {  message: '' };
  this.notificationError= {  message: '' };
  this.notificationSuccess= {  message: '' };
  this.notificationUser= {  message: '' };
 }
onReset() {
  this.hideNotification();
  this.submitted = false;
  this.changePassswordForm.reset();
}
}


export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
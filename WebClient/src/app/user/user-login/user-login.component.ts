import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, GoogleLoginProvider, SocialLoginModule } from 'angularx-social-login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { MessageService } from 'src/app/shared/services/message.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
  providers: [SocialLoginModule, AuthService]
})
export class UserLoginComponent implements OnInit {

  jwt: string;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  notification:any;
  infoMessage='';
  constructor(private socialAuthService: AuthService,
    private userService: UserService,
    private router: Router,
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private routeParam: ActivatedRoute

  ) { }

  ngOnInit() {
      this.routeParam.queryParams
      .subscribe(params => {
        if(params.registered !== undefined && params.registered === 'true') {
          console.log(params.registered);
          this.infoMessage = 'Đăng ký thành công! Vui lòng đăng nhập!';
      }
      });
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
  });
  }
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    this.loading = true;
    this.userService.login(this.loginForm.value).subscribe((res : any)=>{
     console.log(res);
      localStorage.setItem("currentUser",JSON.stringify(res.body));
      localStorage.setItem("auth_token", res.body.token);
      this.router.navigateByUrl('/home');
    },(err: any)=>{
      this.loading = false;
      this.showNotification();
      //alert('Đã có lỗi xảy ra trong quá trình đăng nhập, bạn vui lòng thử lại! ');
    });
}
showNotification(){
 this.notification= {  message: 'Tên đăng nhập hoặc mật khẩu không đúng!' };
}
hideNotification(){
  this.notification= {  message: '' };
 }
onReset() {
  this.hideNotification();
  this.submitted = false;
  this.loginForm.reset();
}
  socialLogin() {
    let socialPlatformProvider;
    socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (res: any) => { //res is the data which is returned by Google User Api
        console.log(res);
        this.userService.googleLogin(res.idToken).then((res: any) => {
          this.jwt = res.token;
          localStorage.setItem("auth_token", res.token);
          localStorage.setItem("currentUser", JSON.stringify(res));
          this.messageService.sendMessage({ reloadUser: true });
          this.router.navigateByUrl('/home');
        });

      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    notification:any;
    loading = false;
    constructor(
        private formBuilder: FormBuilder,
                private  userService: UserService,
             private router: Router) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phone: ['',[Validators.required,  Validators.pattern("^[0-9]*$"),
            Validators.minLength(10), Validators.maxLength(10)]],
            password: ['', [Validators.required,Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),Validators.minLength(8)]],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
    }

    // convenience getter for easy access to form fields
    get f() {        return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.loading = true;
      this.userService.register(this.registerForm.value).subscribe((res : any)=>{
           // console.log(res)
            // display form values on success
         this.router.navigate(['login'], { queryParams: { registered: 'true' } });

           // this.router.navigateByUrl('/login');
          },(err:any)=>{
            this.loading = false;
            this.showNotification();
          }); 
    }
    showNotification(){
        this.notification= {  message: 'Email đã được đăng ký!' };
       }
    hideNotification(){
        this.notification= {  message: '' };
       }
    onReset() {
        this.hideNotification();
        this.submitted = false;
        this.registerForm.reset();
    }
}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/shared/services/user.service';
import { TutorService } from 'src/app/shared/services/tutor.service';
@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  public user:any ;
  public noData = 'Chưa cập nhật';
  public changeImage = false;
  public avatar='';
  public address = '';
  imageUrl: string = "";
  fileToUpload: File = null;
public tutorAvatar= '';
public tutorCertificate='';
public tutorIdentityCard='';
public imageArr = [];
  
  public tutor:any;

  dateofWeek = ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ nhật'];
  day = [0, 0, 0, 0, 0, 0, 0];
  tutorAddress = '';
  constructor(private router: Router
    , private userService:UserService
    , private tutorService : TutorService) {}
  ngOnInit() {
    this.userService.getGetUserInfo().subscribe((data :any) => {
      this.user = data.body;
      var res = this.user.avatar.substring(0, 3);
      if(res == 'http')
      {
        this.avatar = this.user.avatar;
      }
      else{
        this.avatar =  environment.urlImage+this.user.avatar;
      }     
      
      this.imageUrl=this.avatar;
      if(this.user.addressView){
if(this.user.addressView.houseNumber){
  this.address = this.user.addressView.houseNumber;
}
  this.address += ' ' + this.user.addressView.street+ ' '+ this.user.addressView.commune+ ' ' + this.user.addressView.district
  + ' ' + this.user.addressView.city;
}
  });

  this.tutorService.getDetailTutor().subscribe((res:any) => {
    this.tutor = res.body;
  this.imageArr = this.tutor.certificationImageViews;
  
  if(this.imageArr.length> 0){
    for(var i =0; i<this.imageArr.length; i++ ){
      switch(this.imageArr[i].imageType){
        case('IdentityCard'):
        this.tutorIdentityCard = environment.urlImage+this.tutor.certificationImageViews[i].imagePath;
        break;
        case('Avatar'):
        this.tutorAvatar = environment.urlImage+this.tutor.certificationImageViews[i].imagePath;
        break;
        case('IdentityCard'):
        this.tutorIdentityCard = environment.urlImage+this.tutor.certificationImageViews[i].imagePath;
        break;
        default: break;
      }
    }
  }
    this.day[0] = this.tutor.scheduleView.monday;
    this.day[1] = this.tutor.scheduleView.tuesday;
    this.day[2] = this.tutor.scheduleView.wednesday;
    this.day[3] = this.tutor.scheduleView.thursday;
    this.day[4] = this.tutor.scheduleView.friday;
    this.day[5] = this.tutor.scheduleView.saturday;
    this.day[6] = this.tutor.scheduleView.sunday;

    if (this.tutor.address) {
      if (this.tutor.address.houseNumber) {
        this.dateofWeek += this.tutor.address.houseNumber;
      }
      if (this.tutor.address.street) {
        this.tutorAddress += '  ' + this.tutor.address.street;
      }
      if (this.tutor.address.commune) {
        this.tutorAddress += ' ' + this.tutor.address.commune;
      }
      if (this.tutor.address.district) {
        this.tutorAddress += ' ' + this.tutor.address.district;
      }
      if (this.tutor.address.city) {
        this.tutorAddress += ' ' + this.tutor.address.city;
      }
    }else{
      this.tutorAddress='Chưa cập nhật';
    }
},(err:any)=>{
  console.log(err);
});

  }
  handleFileInput(file: FileList) {
    this.changeImage = true;
    this.fileToUpload = file.item(0);

    //Show image preview
    var reader = new FileReader();
    reader.onload = (event:any) => {
      this.imageUrl = event.target.result;
      this.avatar = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  OnSubmit(Image){
   this.userService.postFile(this.fileToUpload).subscribe(
     data =>{
       Image.value = null;
       this.imageUrl = this.user.avatar;
       this.changeImage = false;
     }
   );
  }
  
}

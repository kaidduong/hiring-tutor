﻿using AutoMapper;
using HiringTutors.Core.Entities;
using HiringTutors.Core.Entities.EntityRelationships;
using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Helpers;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services
{
    public class TutorService : ITutorService
    {
        private readonly ITutorRepository _tutorRepository;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAddressRepository _addressRepository;
        private readonly ICommonService _commonService;
        private readonly ISubjectRepository _subjectRepository;
        private readonly INewsRepository _newsRepository;

        public TutorService(ITutorRepository tutorRepository,
                            IMapper mapper,
                            UserManager<ApplicationUser> userManager,
                            IAddressRepository addressRepository,
                            ICommonService commonService,
                            ISubjectRepository subjectRepository,
                            INewsRepository newsRepository)
        {
            _tutorRepository = tutorRepository;
            _mapper = mapper;
            _userManager = userManager;
            _addressRepository = addressRepository;
            _subjectRepository = subjectRepository;
            _commonService = commonService;
            _newsRepository = newsRepository;
        }

        public TutorViewModel AddTutor(ClaimsPrincipal User)
        {
            var user = _userManager.GetUserAsync(User).Result;

            //check user was registerd tutor before
            var tutorExist = _tutorRepository.List(t => t.UserId == user.Id && !t.IsDeleted).FirstOrDefault();
            if (tutorExist == null)
            {
                var tutorView = new CreateTutorView
                {
                    UserId = User.GetUserId()
                };

                var tutor = _mapper.Map<Tutor>(tutorView);
                tutor.CreatedBy = User.Identity.Name;
                tutor.UpdatedOn = DateTime.Now;
                var viewTutor = _mapper.Map<TutorViewModel>(_tutorRepository.Add(tutor));
                viewTutor.Name = user.FullName;
                viewTutor.Email = user.Email;
                viewTutor.Phone = user.PhoneNumber;
                viewTutor.Avatar = user.Avatar;
                viewTutor.Address = user.AddressId == null ? null : _mapper.Map<AddressViewModel>(_addressRepository.GetById((int)user.AddressId));
                viewTutor.ScheduleView = null;
                viewTutor.CertificationImageViews = null;
                viewTutor.Subjects = null;
                _userManager.AddToRoleAsync(user, Roles.TUTOR_NEW);
                return viewTutor;
            }
            return this.GetById(tutorExist.Id);
        }

        public TutorViewModel GetById(int id)
        {
            var tutor = _tutorRepository.ListNavi(t => t.Id == id && !t.IsDeleted).FirstOrDefault();
            if (tutor == null)
            {
                return null;
            }
            else
            {
                var user = _userManager.Users.Where(u => u.Id.Equals(tutor.UserId)).FirstOrDefault();
                var tutorView = _mapper.Map<TutorViewModel>(tutor);
                tutorView.Name = user.FullName;
                tutorView.Email = user.Email;
                tutorView.Avatar = user.Avatar;
                tutorView.Phone = user.PhoneNumber;
                tutorView.Address = user.AddressId == null ? null : _mapper.Map<AddressViewModel>(_addressRepository.GetById((int)user.AddressId));
                if (tutor.TutorSubjects.Count > 0)
                {
                    tutorView.Subjects = new List<SubjectView>();
                    // get subjects
                    foreach (var ts in tutor.TutorSubjects.ToList())
                    {
                        tutorView.Subjects
                            .Add(_subjectRepository
                            .List(s => s.Id == ts.SubjectId)
                            .Select(s => new SubjectView
                            {
                                Id = ts.SubjectId,
                                Name = s.Name
                            })
                            .FirstOrDefault());
                    }
                }
                tutorView.ScheduleView = _mapper.Map<ScheduleViewModel>(tutor.Schedule);
                tutorView.CertificationImageViews = tutor.CertificationImages.Count < 1
                    ? null :
                    tutor.CertificationImages.
                        Select(i => new ImageView
                        {
                            ImagePath = i.Image,
                            ImageType = i.Name
                        }).ToList();
                return tutorView;
            }
        }

        public TutorViewModel GetTutorInfor(string userId)
        {
            var tutorId = _tutorRepository.ListNavi(t => t.UserId.Equals(userId) && !t.IsDeleted).Select(t => t.Id).FirstOrDefault();
            return this.GetById(tutorId);
        }

        public List<TutorViewModel> GetAll()
        {
            var ids = _tutorRepository.List(t => !t.IsDeleted).Select(t => t.Id).ToList();
            var tutors = new List<TutorViewModel>();
            foreach (var id in ids)
            {
                tutors.Add(this.GetById(id));
            }
            return tutors;
        }

        public void Remove(int id, ClaimsPrincipal User)
        {
            var tutor = _tutorRepository.List(t => t.Id == id && !t.IsDeleted).FirstOrDefault();
            if (tutor != null)
            {
                tutor.IsDeleted = true;
                tutor.UpdatedBy = User.Identity.Name;
                tutor.UpdatedOn = DateTime.Now;
                _tutorRepository.UpdateAsync(tutor).Wait();
            }
        }

        public List<TutorViewModel> ListTutors(int pageNum = 1, int pageSize = 15)
        {
            pageNum = pageNum < 0 ? 1 : pageNum;
            pageSize = pageSize < 0 ? 1 : pageSize;
            var offset = pageSize * (pageNum - 1);
            var ids = _tutorRepository.ListNavi(t => !t.IsDeleted && t.Status == (int)TUTOR_STATUS.ACTIVE)
                 .Skip(offset)
                 .Take(pageSize)
                 .OrderByDescending(x => x.Id)
                 .Select(t => t.Id).ToList();

            var tutors = new List<TutorViewModel>();
            foreach (var id in ids)
            {
                tutors.Add(this.GetById(id));
            }
            return tutors;
        }

        public void UpdateInfoTutor(InforTutorView inforTutorView, ClaimsPrincipal User)
        {
            var tutorId = _tutorRepository.ListNavi(t => t.UserId.Equals(User.GetUserId())).Select(t => t.Id).FirstOrDefault();
            var tutor = _tutorRepository.GetById(tutorId);
            tutor.Introduction = inforTutorView.Introduction;
            tutor.Job = inforTutorView.Job;
            tutor.TeachingType = inforTutorView.TeachingType;
            tutor.TuitionPerHour = inforTutorView.TuitionPerHour;
            _tutorRepository.UpdateAsync(tutor).Wait();
            var user = _userManager.FindByIdAsync(User.GetUserId()).Result;
            user.FullName = inforTutorView.Name;
            user.PhoneNumber = inforTutorView.Phone;
            _userManager.UpdateAsync(user).Wait();
        }

        public void UpdateTutor(UpdateTutorView updateTutorView, ClaimsPrincipal User)
        {
            var tutorId = _tutorRepository.ListNavi(t => t.UserId.Equals(User.GetUserId())).Select(t => t.Id).FirstOrDefault();
            var tutor = _tutorRepository.ListNavi(t => t.Id == tutorId).FirstOrDefault();
            // update info
            if (updateTutorView.HasUpdateInfo)
            {
                this.UpdateInfoTutor(updateTutorView.UpdateInforTutorView, User);
            }
            // update subject
            if (updateTutorView.HasUpdateSubject)
            {
                //code
                if (tutor.TutorSubjects.Count < 1)
                {
                    foreach (var subjectId in updateTutorView.SubjectIds)
                    {
                        tutor.TutorSubjects.Add(new TutorSubject
                        {
                            TutorId = tutor.Id,
                            SubjectId = subjectId
                        });
                    }
                }
                else
                {
                    var tsExist = tutor.TutorSubjects.Select(ts => ts.SubjectId).ToList();
                    var tsRemove = tsExist.Except(updateTutorView.SubjectIds);
                    var tsAdding = updateTutorView.SubjectIds.Except(tsExist);

                    //adding
                    foreach (var subjectId in tsAdding)
                    {
                        tutor.TutorSubjects.Add(new TutorSubject
                        {
                            TutorId = tutor.Id,
                            SubjectId = subjectId
                        });
                    }
                    //remove
                    foreach (var subjectId in tsRemove)
                    {
                        var tsE = tutor.TutorSubjects.Where(ts => ts.SubjectId == subjectId).FirstOrDefault();
                        tutor.TutorSubjects.Remove(tsE);
                    }
                }
            }

            //update address
            if (updateTutorView.HasUpdateAddress)
            {
                _commonService.UpdateAddress(updateTutorView.UpdateAddressView, User);
            }
            //update SCHEDULE
            if (updateTutorView.HasUpdateSchedule)
            {
                _commonService.UpdateSchedule(updateTutorView.UpdateScheduleView, User);
            }
            if (tutor.Status != (int)TUTOR_STATUS.ACTIVE)
            {
                tutor.Status = (int)TUTOR_STATUS.WAIT_TO_VERIFY;
            }
            tutor.UpdatedBy = User.Identity.Name;
            tutor.UpdatedOn = DateTime.Now;
            _tutorRepository.UpdateAsync(tutor).Wait();
        }

        public void ConfirmInfoTutor(ConfirmTutorView confirmTutorView, ClaimsPrincipal User)
        {
            var tutor = _tutorRepository.GetById(confirmTutorView.Id);
            tutor.Status = confirmTutorView.ConfirmedType;
            tutor.UpdatedBy = User.Identity.Name;
            tutor.UpdatedOn = DateTime.Now;
            _tutorRepository.UpdateAsync(tutor).Wait();
            if (confirmTutorView.ConfirmedType == (int)TUTOR_STATUS.ACTIVE)
            {
                var user = _userManager.FindByIdAsync(tutor.UserId).Result;
                _userManager.RemoveFromRoleAsync(user, Roles.TUTOR_NEW).Wait();
                _userManager.AddToRoleAsync(user, Roles.TUTOR).Wait();
            }
        }

        public bool OfferToTeach(int newsId, ClaimsPrincipal User)
        {
            var news = _newsRepository.ListNavi(n => n.Id == newsId && n.Status == (int)NEWS_STATUS.ACTIVE && !n.IsDeleted).FirstOrDefault();
            if (news.UserId.Contains(User.GetUserId()))
            {
                return false;
            }
            var tutorId = _tutorRepository.List(t => t.UserId.Contains(User.GetUserId())).Select(t => t.Id).FirstOrDefault();
            var tutor = _tutorRepository.ListNavi(t => t.Id == tutorId && t.Status == (int)TUTOR_STATUS.ACTIVE && !t.IsDeleted).FirstOrDefault();
            if (tutor == null || news == null || news.TutorNews.Select(tn => tn.TutorId).Contains(tutorId))
            {
                return false;
            }
            else
            {
                news.TutorNews.Add(new TutorNews
                {
                    TutorId = tutor.Id,
                    NewsId = news.Id,
                    Status = (int)TUTOR_NEWS_STATUS.NEW
                });
                news.UpdatedBy = User.Identity.Name;
                news.UpdatedOn = DateTime.Now;
                _newsRepository.UpdateAsync(news).Wait();

                tutor.TutorNews.Add(news.TutorNews.Where(tn => tn.TutorId == tutor.Id).FirstOrDefault());
                tutor.UpdatedOn = DateTime.Now;
                tutor.UpdatedBy = User.Identity.Name;
                _tutorRepository.UpdateAsync(tutor).Wait();
                return true;
            }
        }

        public bool CancelTeaching(int newsId, ClaimsPrincipal User)
        {
            var news = _newsRepository.ListNavi(n => n.Id == newsId && n.Status == (int)NEWS_STATUS.ACTIVE && !n.IsDeleted).FirstOrDefault();

            var tutorId = _tutorRepository.List(t => t.UserId.Contains(User.GetUserId())).Select(t => t.Id).FirstOrDefault();

            if (tutorId < 1 || news == null)
            {
                return false;
            }
            else
            {
                news.TutorNews.Where(tn => tn.TutorId == tutorId).FirstOrDefault().Status = (int)TUTOR_NEWS_STATUS.CANCEL_BY_TUTOR;
                news.TutorNews.Where(ts => ts.TutorId == tutorId).FirstOrDefault().UpdatedBy = User.Identity.Name;
                news.TutorNews.Where(ts => ts.TutorId == tutorId).FirstOrDefault().UpdatedOn = DateTime.Now;
                _newsRepository.UpdateAsync(news);
                return true;
            }
        }
    }
}
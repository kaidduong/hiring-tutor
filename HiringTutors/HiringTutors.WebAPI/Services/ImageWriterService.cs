﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HiringTutors.Core.Entities;
using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Helper;
using HiringTutors.WebAPI.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HiringTutors.WebAPI.Services
{
    public class ImageWriterService : IImageWriterService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ITutorRepository _tutorRepository;
        public ImageWriterService(UserManager<ApplicationUser> userManager, ITutorRepository tutorRepository)
        {
            _userManager = userManager;
            _tutorRepository = tutorRepository;
        }

        public async Task<string> UploadImage(IFormFile file, string path, string userId)
        {
            if (CheckIfImageFile(file))
            {
                return await WriteFile(file, path, userId);
            }

            return "Invalid image file";
        }

        /// <summary>
        /// Method to check if file is image file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckIfImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return ImageWriterHelper.GetImageFormat(fileBytes) != ImageWriterHelper.ImageFormat.unknown;
        }

        private string checkIfUpdateAvatar(string userId)
        {
            string rs = "";
            var userInfor = _userManager.Users.Include(u => u.ApiFunctionUsers).FirstOrDefault(u => u.Id == userId);
            var avatar = userInfor.Avatar;
            string[] arr = avatar.Split('/');
            if (arr[2] == "UserAvatar")
            {
                rs = arr[3];
            }
            return rs;
        }

        /// <summary>
        /// Method to write file onto the disk
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<string> WriteFile(IFormFile file, string pathName, string userId)
        {
            string fileName;
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                                                                  //for the file due to security reasons.
                var path = Path.Combine(Directory.GetCurrentDirectory(), "Resources\\Images\\"+ pathName, fileName);

                using (var bits = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(bits);
                }

              
                // update avatarUser
                if (pathName == "UserAvatar")
                {
                    // delete old avatar
                    string oldName = checkIfUpdateAvatar(userId);
                    if (oldName != "")
                    {
                        var oldPath = Path.Combine(Directory.GetCurrentDirectory(), "Resources\\Images\\UserAvatar", oldName);
                        if (System.IO.File.Exists(oldPath))
                        {
                            try
                            {
                                System.IO.File.Delete(oldPath);
                            }
                            catch (System.IO.IOException e)
                            {
                                Console.WriteLine(e.Message);
                            }
        }
                    }
                    var userInfor = _userManager.Users.Include(u => u.ApiFunctionUsers).FirstOrDefault(u => u.Id == userId);
                    userInfor.Avatar = "Resources/Images/" + pathName + "/" + fileName;
                    _userManager.UpdateAsync(userInfor).Wait();
                }
                // update TutorCertificate
                else
                {
                    var tutorId = _tutorRepository.ListNavi(t => t.UserId.Equals(userId)).Select(t => t.Id).FirstOrDefault();
                    var tutor = _tutorRepository.ListNavi(t => t.Id == tutorId).FirstOrDefault();

                    string[] arr = pathName.Split('\\');
                    if(arr.Length == 3)
                    {
                        tutor.CertificationImages.Add(new CertificationImage
                        {
                            TutorId = tutor.Id,
                            Name = arr[2],
                            Image = "Resources/Images/" + arr[0] + "/" + arr[2] + "/" + fileName,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now
                        });
                        _tutorRepository.UpdateAsync(tutor).Wait();
                    }
                    }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return fileName;
        }
    }
}

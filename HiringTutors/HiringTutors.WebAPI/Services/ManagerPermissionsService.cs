﻿using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System.Security.Claims;
using HiringTutors.WebAPI.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HiringTutors.WebAPI.Services
{
    public class ManagerPermissionsService : IManagerPermissionsService
    {
        private IApiFunctionRepository _apiFunctionRepository;
        private readonly IApiDescriptionGroupCollectionProvider _apiExplorer;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ManagerPermissionsService(IApiFunctionRepository apiFunctionRepository,
                                         IApiDescriptionGroupCollectionProvider apiExplorer,
                                         RoleManager<ApplicationRole> roleManager,
                                         UserManager<ApplicationUser> userManager)
        {
            _apiFunctionRepository = apiFunctionRepository;
            _apiExplorer = apiExplorer;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public ApiFunction Add(ApiFunction apiFunction)
        {
            return _apiFunctionRepository.Add(apiFunction);
        }

        public IEnumerable<ApiDescriptionGroupsView> GetAllPermissons()
        {
            var items = _apiExplorer.ApiDescriptionGroups.Items.Select(x => new
            {
                x.GroupName,
                Items = x.Items.Select(y => new ApiFunctionViewModel
                {
                    HttpMethod = y.HttpMethod,
                    RelativePath = y.RelativePath,
                    ControllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)y.ActionDescriptor).ControllerName,
                    ActionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)y.ActionDescriptor).ActionName
                })
            });
            var apis = new List<ApiFunctionViewModel>();
            foreach (var item in items)
            {
                apis.AddRange(item.Items);
            }
            var nodes = apis.GroupBy(x => x.ControllerName).Select(x => new ApiDescriptionGroupsView
            {
                Key = x.Key,
                Items = apis.Where(z => z.ControllerName.Equals(x.Key))
            });

            return nodes;
        }

        public IOrderedQueryable<ApiFunction> GetList(int pageNum = 1, int pageSize = 15)
        {
            pageNum = pageNum < 0 ? 1 : pageNum;
            pageSize = pageSize < 0 ? 1 : pageSize;
            var offset = pageSize * (pageNum - 1);
            return _apiFunctionRepository.List(x => !x.IsDeleted).Skip(offset).Take(pageSize).OrderByDescending(x => x.Id);
        }

        public List<ApplicationRole> GetRoles(string userId)
        {
            return _apiFunctionRepository.GetRoles(userId);
        }

        public IQueryable<ApiFunction> List(Expression<Func<ApiFunction, bool>> predicate)
        {
            return _apiFunctionRepository.List(predicate);
        }

        public void Update(ApiFunction apiFunction)
        {
            _apiFunctionRepository.Update(apiFunction);
        }

        public void MultiCreatePermission(ClaimsPrincipal User)
        {
            var items = _apiExplorer.ApiDescriptionGroups.Items.Select(x => new
            {
                x.GroupName,
                Items = x.Items.Select(y => new ApiFunctionViewModel
                {
                    HttpMethod = y.HttpMethod,
                    RelativePath = y.RelativePath,
                    ControllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)y.ActionDescriptor).ControllerName,
                    ActionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)y.ActionDescriptor).ActionName
                })
            });
            var apis = new List<ApiFunctionViewModel>();
            foreach (var item in items)
            {
                foreach (var citem in item.Items)
                {
                    if (!this.List(x => !x.IsDeleted && x.RelativePath.Equals(citem.RelativePath) && x.HttpMethod.Equals(citem.HttpMethod)).Any())
                    {
                        this.Add(new ApiFunction
                        {
                            ActionName = citem.ActionName,
                            ControllerName = citem.ControllerName,
                            HttpMethod = citem.HttpMethod,
                            RelativePath = citem.RelativePath,
                            CreatedBy = User.Identity.Name,
                            CreatedOn = DateTime.Now
                        });
                    }
                }
            }
        }

        public bool RemovePermission(ApiFunctionEditViewModel apiFunctionEditView)
        {
            var existapiFunction = this.List(x => !x.IsDeleted && x.Id == apiFunctionEditView.Id).FirstOrDefault();
            if (existapiFunction == null)
            {
                return false;
            }
            else
            {
                existapiFunction.IsDeleted = true;
                this.Update(existapiFunction);
                return true;
            }
        }

        public IEnumerable<PermissionView> GetPermissionByRole(ApplicationRole existRole)
        {
            var apiFunctions = this.List(x => !x.IsDeleted).Select(x => new ApiFunctionModel
            {
                ActionName = x.ActionName,
                ApiFunctionId = x.Id,
                ControllerName = x.ControllerName,
                RelativePath = x.RelativePath,
                HttpMethod = x.HttpMethod
            }).ToList();
            foreach (var item in apiFunctions)
            {
                item.IsSelected = existRole.ApiFunctionRoles.Any(x => x.ApiFunctionId == item.ApiFunctionId);
            }
            var nodes = apiFunctions.GroupBy(x => x.ControllerName).Select(x => new PermissionView
            {
                Key = x.Key,
                IsSelected = !apiFunctions.Any(z => z.ControllerName.Equals(x.Key) && !z.IsSelected),
                Items = apiFunctions.Where(z => z.ControllerName.Equals(x.Key))
            });
            return nodes;
        }

        public IEnumerable<PermissionView> GetPermissionByUser(ApplicationUser existUser)
        {
            var existRoles = this.GetRoles(existUser.Id).Select(x => x).ToList();

            var apiFunctions = this.List(x => !x.IsDeleted).Select(x => new ApiFunctionModel
            {
                ActionName = x.ActionName,
                ApiFunctionId = x.Id,
                ControllerName = x.ControllerName,
                RelativePath = x.RelativePath,
                HttpMethod = x.HttpMethod
            }).ToList();
            foreach (var item in apiFunctions)
            {
                item.IsSelected = existRoles.Any(x => x.ApiFunctionRoles.Any(y => y.ApiFunctionId == item.ApiFunctionId));
            }
            foreach (var item in existUser.ApiFunctionUsers)
            {
                var a = apiFunctions.FirstOrDefault(x => x.ApiFunctionId == item.ApiFunctionId);
                if (a != null)
                {
                    a.IsSelected = item.Status == (int)API_ROLE_STATUS.ALLOW ? true : false;
                }
                else
                {
                    var b = apiFunctions.FirstOrDefault(x => x.ApiFunctionId == item.ApiFunctionId);
                    apiFunctions.Add(new ApiFunctionModel
                    {
                        ActionName = b.ActionName,
                        ApiFunctionId = item.ApiFunctionId,
                        ControllerName = b.ControllerName,
                        RelativePath = b.RelativePath,
                        HttpMethod = b.HttpMethod,
                        IsSelected = item.Status == (int)API_ROLE_STATUS.ALLOW ? true : false
                    });
                }
            }
            var nodes = apiFunctions.GroupBy(x => x.ControllerName).Select(x => new PermissionView
            {
                Key = x.Key,
                IsSelected = !apiFunctions.Any(z => z.ControllerName.Equals(x.Key) && !z.IsSelected),
                Items = apiFunctions.Where(z => z.ControllerName.Equals(x.Key))
            });
            return nodes;
        }

        public bool AddPermissionsToRole(ApiFunctionCreateViewModel apiFunctionCreate)
        {
            var existRole = _roleManager.Roles.Include(x => x.ApiFunctionRoles).FirstOrDefault(x => x.Id.Equals(apiFunctionCreate.RoleId));
            if (existRole == null)
            {
                return false;
            }
            else
            {
                foreach (var item in apiFunctionCreate.ApiFunctions)
                {
                    existRole.ApiFunctionRoles.Add(new ApiFunctionRole
                    {
                        ApiFunctionId = item,
                        RoleId = existRole.Id
                    });
                }
                _roleManager.UpdateAsync(existRole).Wait();
                return true;
            }
        }

        public void UpdatePermissionToUser(UpdateFunction updateFunction, ApplicationUser existUser)
        {
            var existApiFunctionUser = existUser.ApiFunctionUsers.FirstOrDefault(x => x.ApiFunctionId == updateFunction.ApiFunctionId);
            if (existApiFunctionUser == null)
            {
                existUser.ApiFunctionUsers.Add(new ApiFunctionUser
                {
                    Status = updateFunction.Status,
                    UserId = existUser.Id,
                    ApiFunctionId = updateFunction.ApiFunctionId
                });
            }
            else
            {
                existApiFunctionUser.Status = updateFunction.Status;
            }

            _userManager.UpdateAsync(existUser).Wait();
        }

        public bool EditRolesOfUser(EditUserRoleViewModel userRoleViewModel)
        {
            var user = _userManager.FindByIdAsync(userRoleViewModel.UserId).Result;

            if (user == null) return false;

            var oldRoles = _userManager.GetRolesAsync(user).Result;

            var deletedRoles = oldRoles.Except(userRoleViewModel.Role);
            var addingRoles = userRoleViewModel.Role.Except(oldRoles);

            if (deletedRoles.Count() > 0)
            {
                _userManager.RemoveFromRolesAsync(user, deletedRoles).Wait();
            }

            if (addingRoles.Count() > 0)
            {
                _userManager.AddToRolesAsync(user, addingRoles).Wait();
            }
            return true;
        }

        public List<PermissionByGroupView> GetPermissionByGroups()
        {
            var roles = _roleManager.Roles.Include(r => r.ApiFunctionRoles).Select(r => new PermissionByGroupView
            {
                GroupName = r.Name,
                Permissions = r.ApiFunctionRoles.Select(p => p.ApiFunction).Select(a => new PermissionViewModel
                {
                    Id = a.Id,
                    ActionName = a.ActionName,
                    ControllerName = a.ControllerName,
                    HttpMethod = a.HttpMethod,
                    RelativePath = a.RelativePath
                }).ToList()
            }).ToList();
            return roles;
        }

        public void CreateRole(CreateRoleView createRoleView, ClaimsPrincipal User)
        {
            var role = new ApplicationRole
            {
                Name = createRoleView.RoleName,
                Description = createRoleView.Description,
                CreatedOn = DateTime.Now,
                CreatedBy = User.Identity.Name
            };
            _roleManager.CreateAsync(role).Wait();
        }
    }
}
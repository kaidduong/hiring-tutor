﻿using AutoMapper;
using HiringTutors.Core.Entities;
using HiringTutors.Core.Entities.EntityRelationships;
using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Helpers;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly ICommonService _commonService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ISubjectRepository _subjectRepository;
        private readonly ITutorRepository _tutorRepository;

        public NewsService(INewsRepository newsRepository,
                            ICommonService commonService,
                            UserManager<ApplicationUser> userManager,
                            IMapper mapper,
                            ISubjectRepository subjectRepository,
                            ITutorRepository tutorRepository
                            )
        {
            _newsRepository = newsRepository;
            _commonService = commonService;
            _mapper = mapper;
            _userManager = userManager;
            _subjectRepository = subjectRepository;
            _tutorRepository = tutorRepository;
        }

        public NewsViewModel CreateNews(CreateNewsView createNewsView, ClaimsPrincipal User)
        {
            // create address
            var address = _commonService.CreateAddress(createNewsView.CreateAddressView, User);

            //create schedule
            var schedule = _commonService.AddSchedule(createNewsView.CreateScheduleView, User);

            // create info news
            var news = _mapper.Map<News>(createNewsView.InforNewsView);
            news.UserId = User.GetUserId();
            news.AddressId = address.Id;
            news.ScheduleId = schedule.Id;
            news.AdmissionFee = Newses.DEFAULT_ADDMISION_FEE;
            news.CreatedBy = User.Identity.Name;
            news.CreatedOn = DateTime.Now;
            news.Status = (int)NEWS_STATUS.NEW;
            news = _newsRepository.Add(news);
            // create subjects
            var subjects = new List<SubjectView>();
            if (createNewsView.SubjectIds.Count > 0)
            {
                var newsExist = _newsRepository.List(n => n.Id == news.Id && !n.IsDeleted).Include(n => n.NewsSubjects).FirstOrDefault();
                foreach (var subjectId in createNewsView.SubjectIds)
                {
                    newsExist.NewsSubjects.Add(new NewsSubject
                    {
                        NewsId = news.Id,
                        SubjectId = subjectId
                    });
                }
                _newsRepository.UpdateAsync(newsExist).Wait();
                // get subjects
                foreach (var ts in newsExist.NewsSubjects.ToList())
                {
                    subjects.Add(_subjectRepository.List(s => s.Id == ts.SubjectId && !s.IsDeleted)
                        .Select(s => new SubjectView
                        {
                            Id = ts.SubjectId,
                            Name = s.Name
                        })
                        .FirstOrDefault());
                }
            }

            return new NewsViewModel
            {
                Id = news.Id,
                InforNewsView = createNewsView.InforNewsView,
                AddressView = _mapper.Map<AddressViewModel>(address),
                ScheduleView = _mapper.Map<ScheduleViewModel>(schedule),
                SubjectViews = subjects
            };
        }

        public NewsViewModel GetNewsById(int id)
        {
            var news = _newsRepository.ListNavi(n => n.Id == id && !n.IsDeleted).FirstOrDefault();
            if (news == null)
            {
                return null;
            }
            else
            {
                var subjects = new List<SubjectView>();
                if (news.NewsSubjects.Count > 0)
                {
                    foreach (var ts in news.NewsSubjects.ToList())
                    {
                        subjects.Add(_subjectRepository.List(s => s.Id == ts.SubjectId && !s.IsDeleted)
                            .Select(s => new SubjectView
                            {
                                Id = ts.SubjectId,
                                Name = s.Name
                            })
                            .FirstOrDefault());
                    }
                }
                var res = new NewsViewModel
                {
                    Id = news.Id,
                    InforNewsView = _mapper.Map<InforNewsView>(news),
                    AddressView = _mapper.Map<AddressViewModel>(news.Address),
                    ScheduleView = _mapper.Map<ScheduleViewModel>(news.Schedule),
                    SubjectViews = subjects
                };
                //get list tutor candicate ids
                res.InforNewsView.TutorCandicateIds = news.TutorNews.Select(tn => tn.TutorId).ToList();
                return res;
            }
        }

        public List<NewsViewModel> GetAll()
        {
            var ids = _newsRepository.List(n => !n.IsDeleted).Select(t => t.Id).ToList();
            var newses = new List<NewsViewModel>();
            foreach (var id in ids)
            {
                newses.Add(this.GetNewsById(id));
            }
            return newses;
        }

        public List<NewsViewModel> GetAllNewsByUser(string userId)
        {
            var ids = _newsRepository.List(n => !n.IsDeleted && n.UserId.Contains(userId)).Select(t => t.Id).ToList();
            var newses = new List<NewsViewModel>();
            foreach (var id in ids)
            {
                newses.Add(this.GetNewsById(id));
            }
            return newses;
        }

        public List<NewsViewModel> GetAllActivedNews()
        {
            var ids = _newsRepository.List(n => !n.IsDeleted && n.Status == (int)NEWS_STATUS.ACTIVE).Select(t => t.Id).ToList();
            var newses = new List<NewsViewModel>();
            foreach (var id in ids)
            {
                newses.Add(this.GetNewsById(id));
            }
            newses.ForEach(x => x.InforNewsView.PhoneNumber = null);
            return newses;
        }

        public List<NewsViewModel> ListNewses(int pageNum = 1, int pageSize = 15)
        {
            pageNum = pageNum < 0 ? 1 : pageNum;
            pageSize = pageSize < 0 ? 1 : pageSize;
            var offset = pageSize * (pageNum - 1);
            var ids = _newsRepository.ListNavi(t => !t.IsDeleted && t.Status == (int)NEWS_STATUS.ACTIVE)
                 .Skip(offset)
                 .Take(pageSize)
                 .OrderByDescending(x => x.Id)
                 .Select(t => t.Id).ToList();

            var newses = new List<NewsViewModel>();
            foreach (var id in ids)
            {
                newses.Add(this.GetNewsById(id));
            }
            newses.ForEach(x => x.InforNewsView.PhoneNumber = null);
            return newses;
        }

        public void UpdateInfoNews(int id, UpdateInforNewsView inforNewsView, ClaimsPrincipal User)
        {
            var news = _newsRepository.GetById(id);
            var user = _userManager.FindByIdAsync(User.GetUserId()).Result;
            var roles = _userManager.GetRolesAsync(user).Result;
            if (roles.Contains(Roles.ADMIN))
            {
                news.AdmissionFee = inforNewsView.AdmissionFee;
            }
            news.Content = inforNewsView.Content;
            news.Title = inforNewsView.Title;
            news.TuitionPerHour = inforNewsView.TuitionPerHour;
            news.TutorRequired = inforNewsView.TutorRequired;
            news.TeachingType = inforNewsView.TeachingType;
            news.PhoneNumber = inforNewsView.PhoneNumber;
            news.AmountOfTeachingTimePerLesson = inforNewsView.AmountOfTeachingTimePerLesson;
            news.Status = (int)NEWS_STATUS.WAIT_TO_VERIFY;
            news.UpdatedBy = User.Identity.Name;
            news.UpdatedOn = DateTime.Now;
            _newsRepository.UpdateAsync(news).Wait();
        }

        public void UpdateNews(UpdateNewsView updateNewsView, ClaimsPrincipal User)
        {
            var news = _newsRepository.ListNavi(t => t.Id == updateNewsView.Id && !t.IsDeleted).FirstOrDefault();
            // update info
            if (updateNewsView.HasUpdateInfo)
            {
                this.UpdateInfoNews(news.Id, updateNewsView.UpdateInforNewsView, User);
            }
            // update subject
            if (updateNewsView.HasUpdateSubject)
            {
                //code
                if (news.NewsSubjects.Count < 1)
                {
                    foreach (var subjectId in updateNewsView.SubjectIds)
                    {
                        news.NewsSubjects.Add(new NewsSubject
                        {
                            NewsId = news.Id,
                            SubjectId = subjectId
                        });
                    }
                }
                else
                {
                    var tsExist = news.NewsSubjects.Select(ts => ts.SubjectId).ToList();
                    var tsRemove = tsExist.Except(updateNewsView.SubjectIds);
                    var tsAdding = updateNewsView.SubjectIds.Except(tsExist);

                    //adding
                    foreach (var subjectId in tsAdding)
                    {
                        news.NewsSubjects.Add(new NewsSubject
                        {
                            NewsId = news.Id,
                            SubjectId = subjectId
                        });
                    }
                    //remove
                    foreach (var subjectId in tsRemove)
                    {
                        var tsE = news.NewsSubjects.Where(ts => ts.SubjectId == subjectId).FirstOrDefault();
                        news.NewsSubjects.Remove(tsE);
                    }
                }
            }

            //update address
            if (updateNewsView.HasUpdateAddress)
            {
                _commonService.UpdateAddress(updateNewsView.UpdateAddressView, User);
            }
            //update SCHEDULE
            if (updateNewsView.HasUpdateSchedule)
            {
                _commonService.UpdateSchedule(updateNewsView.UpdateScheduleView, User);
            }
            if (news.Status != (int)TUTOR_STATUS.ACTIVE)
            {
                news.Status = (int)TUTOR_STATUS.WAIT_TO_VERIFY;
            }
            news.UpdatedBy = User.Identity.Name;
            news.UpdatedOn = DateTime.Now;
            _newsRepository.UpdateAsync(news).Wait();
        }

        public void ConfirmInfoNews(ConfirmNewsView confirmNewsView, ClaimsPrincipal User)
        {
            var news = _newsRepository.List(t => t.Id == confirmNewsView.Id && !t.IsDeleted).FirstOrDefault();
            news.Status = confirmNewsView.ConfirmedType;
            news.UpdatedBy = User.Identity.Name;
            news.UpdatedOn = DateTime.Now;
            if (news.Status == (int)NEWS_STATUS.ACTIVE && news.ActivedOn == null)
            {
                news.ActivedOn = DateTime.Now;
            }
            _newsRepository.UpdateAsync(news).Wait();
        }

        public void Remove(int id, ClaimsPrincipal User)
        {
            var news = _newsRepository.List(t => t.Id == id && !t.IsDeleted).FirstOrDefault();
            if (news != null)
            {
                news.IsDeleted = true;
                news.UpdatedBy = User.Identity.Name;
                news.UpdatedOn = DateTime.Now;
                _newsRepository.UpdateAsync(news).Wait();
            }
        }

        public bool CancelCandicates(CancelCandicatesView cancelCandicatesView, ClaimsPrincipal User)
        {
            var news = _newsRepository.ListNavi(n => n.Id == cancelCandicatesView.NewsId &&
                (n.Status == (int)NEWS_STATUS.ACTIVE || n.Status == (int)NEWS_STATUS.HAS_CANDICATES)
                && !n.IsDeleted)
                .FirstOrDefault();

            if (news == null)
            {
                return false;
            }
            else
            {
                foreach (var tutorId in cancelCandicatesView.TutorCandicateIds)
                {
                    news.TutorNews.Where(ts => ts.TutorId == tutorId).FirstOrDefault().Status = (int)TUTOR_NEWS_STATUS.CANCEL_BY_USER;
                    news.TutorNews.Where(ts => ts.TutorId == tutorId).FirstOrDefault().UpdatedBy = User.Identity.Name;
                    news.TutorNews.Where(ts => ts.TutorId == tutorId).FirstOrDefault().UpdatedOn = DateTime.Now;
                }
                _newsRepository.UpdateAsync(news).Wait();
                return true;
            }
        }

        public bool InviteCandicate(CandicateView candicateView, ClaimsPrincipal User)
        {
            var news = _newsRepository.ListNavi(n => n.Id == candicateView.NewsId &&
                 (n.Status == (int)NEWS_STATUS.ACTIVE || n.Status == (int)NEWS_STATUS.HAS_CANDICATES)
                 && !n.IsDeleted)
                 .FirstOrDefault();

            if (news == null || news.Status != (int)NEWS_STATUS.ACTIVE || news.Status != (int)NEWS_STATUS.HAS_CANDICATES)
            {
                return false;
            }
            else
            {
                var tutor = _tutorRepository.ListNavi(t => t.Id == candicateView.TutorId && t.Status == (int)TUTOR_STATUS.ACTIVE && !t.IsDeleted).FirstOrDefault();
                if (tutor == null || news.TutorNews.Select(tn => tn.TutorId).Contains(candicateView.TutorId))
                {
                    return false;
                }
                else
                {
                    news.TutorNews.Add(new TutorNews
                    {
                        TutorId = candicateView.TutorId,
                        NewsId = news.Id,
                        Status = (int)TUTOR_NEWS_STATUS.ACTIVE
                    });
                    news.UpdatedOn = DateTime.Now;
                    news.UpdatedBy = User.Identity.Name;
                    _newsRepository.UpdateAsync(news).Wait();

                    tutor.TutorNews.Add(news.TutorNews.Where(tn => tn.TutorId == tutor.Id).FirstOrDefault());
                    _tutorRepository.UpdateAsync(tutor).Wait();
                    return true;
                }
            }
        }

        public bool ApproveCandicate(CandicateView candicateView, ClaimsPrincipal User)
        {
            var news = _newsRepository.ListNavi(n => n.Id == candicateView.NewsId &&
                (n.Status == (int)NEWS_STATUS.ACTIVE || n.Status == (int)NEWS_STATUS.HAS_CANDICATES)
                && !n.IsDeleted)
                .FirstOrDefault();

            if (news == null || news.Status != (int)NEWS_STATUS.ACTIVE || news.Status != (int)NEWS_STATUS.HAS_CANDICATES)
            {
                return false;
            }
            else
            {
                var tutor = _tutorRepository.ListNavi(t => t.Id == candicateView.TutorId && t.Status == (int)TUTOR_STATUS.ACTIVE && !t.IsDeleted).FirstOrDefault();
                if (tutor == null || !news.TutorNews.Select(tn => tn.TutorId).Contains(candicateView.TutorId))
                {
                    return false;
                }
                else
                {
                    news.TutorNews.Where(ts => ts.TutorId == candicateView.TutorId).FirstOrDefault().Status = (int)TUTOR_NEWS_STATUS.ACTIVE;
                    news.TutorNews.Where(ts => ts.TutorId == candicateView.TutorId).FirstOrDefault().UpdatedBy = User.Identity.Name;
                    news.TutorNews.Where(ts => ts.TutorId == candicateView.TutorId).FirstOrDefault().UpdatedOn = DateTime.Now;
                    return true;
                }
            }
        }
    }
}
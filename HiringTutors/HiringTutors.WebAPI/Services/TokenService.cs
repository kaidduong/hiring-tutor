﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using HiringTutors.WebAPI.ViewModels;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.Helpers;

namespace HiringTutors.WebAPI.Services
{
    public class TokenService : ITokenService
    {
        private readonly IConfiguration _configuration;

        public TokenService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string CreateAccessToken(UserViewModel user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            int tokenTimeLifeMinutes = _configuration.GetSection("JwtAuthSettings").Get<JwtAuthSettings>().TokenTimeLifeMinutes;
            var secret = _configuration.GetSection("JwtAuthSettings").Get<JwtAuthSettings>().Secret;
            var key = Encoding.ASCII.GetBytes(secret);

            var claimsIdentity = new ClaimsIdentity();
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            foreach (var role in user.Roles)
            {
                claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, role));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = "HiringTutors.API",
                Subject = claimsIdentity,
                Expires = DateTime.Now.AddMinutes(tokenTimeLifeMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
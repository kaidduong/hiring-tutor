﻿using AutoMapper;
using HiringTutors.Core.Entities;
using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Helpers;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services
{
    public class CommonService : ICommonService
    {
        private readonly IMapper _mapper;
        private readonly IAddressRepository _addressRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IScheduleRepository _scheduleRepository;
        private readonly ITutorRepository _tutorRepository;
        private readonly ICategoryOfSubjectRepository _categoryOfSubjectRepository;
        private readonly ISubjectRepository _subjectRepository;

        public CommonService(IMapper mapper,
                                IAddressRepository addressRepository,
                                UserManager<ApplicationUser> userManager,
                                IScheduleRepository scheduleRepository,
                                ITutorRepository tutorRepository,
                                ICategoryOfSubjectRepository categoryOfSubjectRepository,
                                ISubjectRepository subjectRepository)
        {
            _mapper = mapper;
            _addressRepository = addressRepository;
            _userManager = userManager;
            _scheduleRepository = scheduleRepository;
            _tutorRepository = tutorRepository;
            _categoryOfSubjectRepository = categoryOfSubjectRepository;
            _subjectRepository = subjectRepository;
        }

        public Address CreateAddress(CreateAddressView addressView, ClaimsPrincipal User)
        {
            var address = _mapper.Map<Address>(addressView);
            address.CreatedBy = User.Identity.Name;
            address.CreatedOn = DateTime.Now;
            return _addressRepository.Add(address);
        }

        public void EditAddress(UpdateAdddressView addressView, ClaimsPrincipal User)
        {
            var address = _addressRepository.GetById(addressView.Id);
            address.HouseNumber = addressView.HouseNumber;
            address.Street = addressView.Street;
            address.District = addressView.District;
            address.City = addressView.City;
            address.Commune = addressView.Commune;
            address.UpdatedBy = User.Identity.Name;
            address.UpdatedOn = DateTime.Now;
            _addressRepository.Update(address);
        }

        public AddressViewModel GetAddress(int id)
        {
            var address = _addressRepository.GetById(id);
            return (_mapper.Map<AddressViewModel>(address));
        }

        public void UpdateAddress(UpdateAdddressView addressView, ClaimsPrincipal User)
        {
            if (addressView.Id < 0)
            {
                var a = _mapper.Map<CreateAddressView>(addressView);
                var address = this.CreateAddress(a, User);
                switch (addressView.TypeUpdate)
                {
                    case (int)TYPE_UPDATE.USER:
                        {
                            var user = _userManager.GetUserAsync(User).Result;
                            user.AddressId = address.Id;
                            user.UpdatedBy = User.Identity.Name;
                            user.UpdatedOn = DateTime.Now;
                            _userManager.UpdateAsync(user).Wait();
                            break;
                        }
                    default:
                        break;
                }
            }
            else
            {
                this.EditAddress(addressView, User);
            }
        }

        public ScheduleViewModel GetSchedule(int id)
        {
            return _mapper.Map<ScheduleViewModel>(_scheduleRepository.List(s => s.Id == id && !s.IsDeleted).FirstOrDefault());
        }

        public ScheduleViewModel AddSchedule(CreateScheduleViewModel createScheduleView, ClaimsPrincipal User)
        {
            var schedule = _mapper.Map<Schedule>(createScheduleView);
            schedule.CreatedBy = User.Identity.Name;
            schedule.CreatedOn = DateTime.Now;

            return _mapper.Map<ScheduleViewModel>(_scheduleRepository.Add(schedule));
        }

        public void EditSchedule(UpdateScheduleViewModel updateScheduleView, ClaimsPrincipal User)
        {
            var schedule = _scheduleRepository.GetById(updateScheduleView.Id);
            schedule.Monday = updateScheduleView.Monday;
            schedule.Tuesday = updateScheduleView.Tuesday;
            schedule.Wednesday = updateScheduleView.Wednesday;
            schedule.Thursday = updateScheduleView.Thursday;
            schedule.Friday = updateScheduleView.Friday;
            schedule.Saturday = updateScheduleView.Saturday;
            schedule.Sunday = updateScheduleView.Sunday;
            schedule.UpdatedBy = User.Identity.Name;
            schedule.UpdatedOn = DateTime.Now;
            _scheduleRepository.UpdateAsync(schedule).Wait();
        }

        public void UpdateSchedule(UpdateScheduleViewModel updateScheduleView, ClaimsPrincipal User)
        {
            if (updateScheduleView.Id < 0)
            {
                var schedule = this.AddSchedule(_mapper.Map<CreateScheduleViewModel>(updateScheduleView), User);
                switch (updateScheduleView.TypeUpdate)
                {
                    case (int)TYPE_UPDATE.TUTOR:
                        {
                            var tutor = _tutorRepository.List(t => t.UserId.Equals(User.GetUserId()) && !t.IsDeleted).FirstOrDefault();
                            tutor.ScheduleId = schedule.Id;
                            tutor.UpdatedBy = User.Identity.Name;
                            tutor.UpdatedOn = DateTime.Now;
                            _tutorRepository.UpdateAsync(tutor).Wait();
                            break;
                        }
                    default:
                        break;
                }
            }
            else
            {
                this.EditSchedule(updateScheduleView, User);
            }
        }

        public CategotyOfSubject CreateCategory(CreateCategoryView createCategoryView, ClaimsPrincipal User)
        {
            var cat = _mapper.Map<CategotyOfSubject>(createCategoryView);
            cat.CreatedBy = User.Identity.Name;
            cat.CreatedOn = DateTime.Now;
            return _categoryOfSubjectRepository.Add(cat);
        }

        public Subject CreateSubject(CreateSubjectViewModel createSubjectView, ClaimsPrincipal User)
        {
            var subject = _mapper.Map<Subject>(createSubjectView);
            subject.CreatedOn = DateTime.Now;
            subject.CreatedBy = User.Identity.Name;
            return _subjectRepository.Add(subject);
        }

        public List<SubjectViewModel> GetSubjects()
        {
            return _categoryOfSubjectRepository.ListNavi(c => !c.IsDeleted)
                                .Select(x => new SubjectViewModel
                                {
                                    CategoryView = new CategoryView { Id = x.Id, Name = x.Name },
                                    Subjects = x.Subjects.Select(s => new SubjectView { Id = s.Id, Name = s.Name }).ToList()
                                }).ToList();
        }
    }
}
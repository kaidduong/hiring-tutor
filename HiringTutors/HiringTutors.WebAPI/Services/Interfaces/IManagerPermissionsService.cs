﻿using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface IManagerPermissionsService
    {
        IOrderedQueryable<ApiFunction> GetList(int pageNum = 1, int pageSize = 15);

        ApiFunction Add(ApiFunction apiFunction);

        void Update(ApiFunction apiFunction);

        IQueryable<ApiFunction> List(Expression<Func<ApiFunction, bool>> predicate);

        List<ApplicationRole> GetRoles(string userId);

        void MultiCreatePermission(ClaimsPrincipal User);

        IEnumerable<ApiDescriptionGroupsView> GetAllPermissons();

        bool RemovePermission(ApiFunctionEditViewModel apiFunctionEditView);

        IEnumerable<PermissionView> GetPermissionByRole(ApplicationRole existRole);

        IEnumerable<PermissionView> GetPermissionByUser(ApplicationUser existUser);

        bool AddPermissionsToRole(ApiFunctionCreateViewModel apiFunctionCreate);

        void UpdatePermissionToUser(UpdateFunction updateFunction, ApplicationUser existUser);

        bool EditRolesOfUser(EditUserRoleViewModel userRoleViewModel);

        List<PermissionByGroupView> GetPermissionByGroups();

        void CreateRole(CreateRoleView createRoleView, ClaimsPrincipal User);
    }
}
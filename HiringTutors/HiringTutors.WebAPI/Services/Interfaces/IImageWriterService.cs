﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface IImageWriterService
    {
        Task<string> UploadImage(IFormFile file, string path, string userId);
    }
}

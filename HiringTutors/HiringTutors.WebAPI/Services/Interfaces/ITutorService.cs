﻿using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface ITutorService
    {
        TutorViewModel AddTutor(ClaimsPrincipal User);

        TutorViewModel GetById(int id);

        List<TutorViewModel> GetAll();

        void Remove(int id, ClaimsPrincipal User);

        void UpdateTutor(UpdateTutorView updateTutorView, ClaimsPrincipal User);

        void UpdateInfoTutor(InforTutorView inforTutorView, ClaimsPrincipal User);

        TutorViewModel GetTutorInfor(string userId);

        void ConfirmInfoTutor(ConfirmTutorView confirmTutorView, ClaimsPrincipal User);

        List<TutorViewModel> ListTutors(int pageNum = 1, int pageSize = 15);

        bool OfferToTeach(int newsId, ClaimsPrincipal User);

        bool CancelTeaching(int newsId, ClaimsPrincipal User);
    }
}
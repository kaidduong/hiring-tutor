﻿using HiringTutors.WebAPI.ViewModels;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface ITokenService
    {
        string CreateAccessToken(UserViewModel user);
    }
}
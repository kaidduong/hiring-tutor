﻿using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface IUserService
    {
        bool CheckUserExist(string userId);

        UserInfoViewModel GetUserInforById(string userId);

        List<UserInfoViewModel> GetAllUsers();

        void EditUserInfo(EditUserInfoView editUserInfoView, ClaimsPrincipal User);
        bool ChangePasswordAsync(ChangePasswordRequest request, ClaimsPrincipal User);
        void RemoveUser(string userId, ClaimsPrincipal User);

        List<UserInfoViewModel> ListUsers(int pageNum = 1, int pageSize = 15);
    }
}
﻿using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface IAuthService
    {
        UserViewModel Register(AddUserView userCreateView);

        UserViewModel LoginByGoogleAccount(LoginViewModel loginViewModel);

        UserViewModel Login(LoginAccountViewModel userLoginView);
    }
}
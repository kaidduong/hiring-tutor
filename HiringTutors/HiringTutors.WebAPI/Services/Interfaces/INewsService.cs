﻿using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface INewsService
    {
        NewsViewModel CreateNews(CreateNewsView createNewsView, ClaimsPrincipal User);

        NewsViewModel GetNewsById(int id);

        List<NewsViewModel> ListNewses(int pageNum = 1, int pageSize = 15);

        void ConfirmInfoNews(ConfirmNewsView confirmNewsView, ClaimsPrincipal User);

        void UpdateNews(UpdateNewsView updateNewsView, ClaimsPrincipal User);

        void UpdateInfoNews(int id, UpdateInforNewsView inforNewsView, ClaimsPrincipal User);

        List<NewsViewModel> GetAll();

        void Remove(int id, ClaimsPrincipal User);

        List<NewsViewModel> GetAllNewsByUser(string userId);

        List<NewsViewModel> GetAllActivedNews();

        bool CancelCandicates(CancelCandicatesView cancelCandicatesView, ClaimsPrincipal User);

        bool InviteCandicate(CandicateView candicateView, ClaimsPrincipal User);

        bool ApproveCandicate(CandicateView candicateView, ClaimsPrincipal User);
    }
}
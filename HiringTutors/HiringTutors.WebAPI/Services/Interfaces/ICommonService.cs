﻿using HiringTutors.Core.Entities;
using HiringTutors.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services.Interfaces
{
    public interface ICommonService
    {
        Address CreateAddress(CreateAddressView addressView, ClaimsPrincipal User);

        void EditAddress(UpdateAdddressView addressView, ClaimsPrincipal User);

        void UpdateAddress(UpdateAdddressView addressView, ClaimsPrincipal User);

        AddressViewModel GetAddress(int id);

        void UpdateSchedule(UpdateScheduleViewModel updateScheduleView, ClaimsPrincipal User);

        void EditSchedule(UpdateScheduleViewModel updateScheduleView, ClaimsPrincipal User);

        ScheduleViewModel AddSchedule(CreateScheduleViewModel createScheduleView, ClaimsPrincipal User);

        List<SubjectViewModel> GetSubjects();

        Subject CreateSubject(CreateSubjectViewModel createSubjectView, ClaimsPrincipal User);

        CategotyOfSubject CreateCategory(CreateCategoryView createCategoryView, ClaimsPrincipal User);
    }
}
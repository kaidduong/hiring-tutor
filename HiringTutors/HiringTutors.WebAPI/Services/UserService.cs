﻿using AutoMapper;
using HiringTutors.Core.Entities;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly ICommonService _commonService;

        public UserService(UserManager<ApplicationUser> userManager,
                            IMapper mapper,
                            RoleManager<ApplicationRole> roleManager,
                            ICommonService commonService)
        {
            _userManager = userManager;
            _mapper = mapper;
            _roleManager = roleManager;
            _commonService = commonService;
        }

        public bool CheckUserExist(string userId)
        {
            return (_userManager.Users.Where(u => u.Id.Equals(userId) && !u.IsDeleted).FirstOrDefault() == null) ? false : true;
        }

        public List<UserInfoViewModel> GetAllUsers()
        {
            var users = _userManager.Users.Where(u => !u.IsDeleted).ToList();
            var usersInfo = new List<UserInfoViewModel>();

            foreach (var user in users)
            {
                var userInfo = _mapper.Map<UserInfoViewModel>(user);
                userInfo.Roles = _userManager.GetRolesAsync(user).Result;
                if (user.AddressId != null)
                {
                    userInfo.AddressView = _commonService.GetAddress((int)user.AddressId);
                }
                usersInfo.Add(userInfo);
            }
            return usersInfo;
        }

        public List<UserInfoViewModel> ListUsers(int pageNum = 1, int pageSize = 15)
        {
            var usersInfo = new List<UserInfoViewModel>();
            pageNum = pageNum < 0 ? 1 : pageNum;
            pageSize = pageSize < 0 ? 1 : pageSize;
            var offset = pageSize * (pageNum - 1);
            var users = _userManager.Users.Where(t => !t.IsDeleted)
                 .Skip(offset)
                 .Take(pageSize)
                 .OrderByDescending(x => x.Id)
                 .ToList();

            foreach (var user in users)
            {
                var userInfo = _mapper.Map<UserInfoViewModel>(user);
                userInfo.Roles = _userManager.GetRolesAsync(user).Result;
                if (user.AddressId != null)
                {
                    userInfo.AddressView = _commonService.GetAddress((int)user.AddressId);
                }
                usersInfo.Add(userInfo);
            }
            return usersInfo;
        }

        public UserInfoViewModel GetUserInforById(string userId)
        {
            var userInfor = _userManager.Users.Include(u => u.ApiFunctionUsers).Where(u => u.Id.Equals(userId)).FirstOrDefault();

            var userInforView = _mapper.Map<UserInfoViewModel>(userInfor);

            userInforView.Roles = _userManager.GetRolesAsync(userInfor).Result;
            if (userInfor.AddressId != null)
            {
                userInforView.AddressView = _commonService.GetAddress((int)userInfor.AddressId);
            }

            return userInforView;
        }

        public void EditUserInfo(EditUserInfoView editUserInfoView, ClaimsPrincipal User)
        {
            var userInfo = _userManager.GetUserAsync(User).Result;
            if (editUserInfoView.HasUpdateAddress)
            {
                _commonService.UpdateAddress(editUserInfoView.AddressView, User);
            }
            if (editUserInfoView.HasUpdateInfoUser)
            {
                userInfo.FullName = editUserInfoView.UserInfoView.FullName;
                userInfo.PhoneNumber = editUserInfoView.UserInfoView.Phone;
                userInfo.UpdatedBy = User.Identity.Name;
                userInfo.UpdatedOn = DateTime.Now;
                _userManager.UpdateAsync(userInfo).Wait();
            }
        }

        public bool ChangePasswordAsync(ChangePasswordRequest request, ClaimsPrincipal User)
        {
            var user = _userManager.GetUserAsync(User).Result;
            var checkPassword = _userManager.CheckPasswordAsync(user, request.OldPassword).Result;
            if (checkPassword)
            {
              var token=  _userManager.GeneratePasswordResetTokenAsync(user).Result;
              _userManager.ResetPasswordAsync(user, token, request.NewPassword).Wait();
                return true;
            }
            else
            {
                return false;
            }
        }

        private object ResetPasswordAsync(ApplicationUser user, Task<string> token, string newPassword)
        {
            throw new NotImplementedException();
        }

        public void RemoveUser(string userId, ClaimsPrincipal User)
        {
            //var users = _userManager.Users.ToList();
            //foreach (var u in users)
            //{
            //    u.IsDeleted = false;
            //    _userManager.UpdateAsync(u).Wait();
            //}
            var user = _userManager.Users.Where(u => !u.IsDeleted && u.Id.Equals(userId)).FirstOrDefault();
            user.IsDeleted = true;
            user.UpdatedBy = User.Identity.Name;
            user.UpdatedOn = DateTime.Now;
            _userManager.UpdateAsync(user).Wait();
        }
    }
}
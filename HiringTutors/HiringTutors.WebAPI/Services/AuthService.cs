﻿using AutoMapper;
using HiringTutors.Core.Interfaces;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Helpers;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Services
{
    public class AuthService : IAuthService
    {
        private readonly ISocialAuthService _socialAuthService;
        private readonly ITokenService _tokenService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public AuthService(ISocialAuthService socialAuthService,
                            ITokenService tokenService,
                            UserManager<ApplicationUser> userManager,
                            IMapper mapper)
        {
            _socialAuthService = socialAuthService;
            _tokenService = tokenService;
            _userManager = userManager;
            _mapper = mapper;
        }

        public UserViewModel LoginByGoogleAccount(LoginViewModel loginViewModel)
        {
            var userViewModel = new UserViewModel();
            // Validate Google token
            _socialAuthService.ValidateToken(loginViewModel.Token);

            if (!_socialAuthService.IsAuthenticated)
            {
                return userViewModel;
            }

            ISocialUserProfile userProfile = _socialAuthService.GetUserProfile();

            var user = _userManager.FindByEmailAsync(userProfile.Email).Result;

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = userProfile.Email.Replace("@gmail.com", ""),
                    FullName = userProfile.Name,
                    Email = userProfile.Email,
                    Avatar = userProfile.Avatar,
                    CreatedOn = DateTime.Now,
                    CreatedBy = "API_AUTO"
                };

                _userManager.CreateAsync(user, "Default@password123").Wait();
                _userManager.AddToRoleAsync(user, Roles.USER).Wait();
            }
            if (user.IsDeleted)
            {
                userViewModel.Id = "NONE";
                return userViewModel;
            }
            userViewModel = _mapper.Map<UserViewModel>(user);
            userViewModel.Roles = _userManager.GetRolesAsync(user).Result;
            userViewModel.Token = _tokenService.CreateAccessToken(userViewModel);
            return userViewModel;
        }

        public UserViewModel Register(AddUserView userCreateView)
        {
            var userViewModel = new UserViewModel();
            var user = _userManager.FindByEmailAsync(userCreateView.Email).Result;
            if (user == null)
            {
                user = new ApplicationUser
                {
                    FullName = userCreateView.Name,
                    PhoneNumber = userCreateView.Phone,
                    Email = userCreateView.Email,
                    Avatar = "Resources/Images/UserAvatarDefault/User-Role-Permission.png",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "API_AUTO",
                    UserName = userCreateView.Email.Split("@")[0]
                };
                _userManager.CreateAsync(user, userCreateView.Password).Wait();
                _userManager.AddToRoleAsync(user, Roles.USER).Wait();
            }
            else
            {
                return userViewModel;
            }

            userViewModel = _mapper.Map<UserViewModel>(user);
            userViewModel.Roles = _userManager.GetRolesAsync(user).Result;
            userViewModel.Token = _tokenService.CreateAccessToken(userViewModel);

            return userViewModel;
        }

        public UserViewModel Login(LoginAccountViewModel userLoginView)
        {
            var userViewModel = new UserViewModel();
            var user = _userManager.FindByEmailAsync(userLoginView.Email).Result;
            if (user != null)
            {
                var checkPassword = _userManager.CheckPasswordAsync(user, userLoginView.Password).Result;

                if (checkPassword)
                {
                    userViewModel = _mapper.Map<UserViewModel>(user);
                    userViewModel.Roles = _userManager.GetRolesAsync(user).Result;
                    userViewModel.Token = _tokenService.CreateAccessToken(userViewModel);

                    return userViewModel;
                }
                else
                {
                    return userViewModel;
                }
            }
            else
            {
                return userViewModel;
            }
        }
    }
}
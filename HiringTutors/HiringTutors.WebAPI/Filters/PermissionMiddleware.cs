﻿using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Filters
{
    public class PermissionMiddleware
    {
        private static readonly RouteData EmptyRouteData = new RouteData();
        private static readonly ActionDescriptor EmptyActionDescriptor = new ActionDescriptor();

        private readonly RequestDelegate _next;

        public PermissionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                if (IsBypassAPI(httpContext.Request.Path.ToString()) || httpContext.Request.Path.StartsWithSegments("/api/ManagerPermissons"))
                {
                    await _next(httpContext);
                }
                else
                {
                    var userManager = (UserManager<ApplicationUser>)httpContext.RequestServices.GetService(typeof(UserManager<ApplicationUser>));
                    var existUser = userManager.Users.Include(x => x.ApiFunctionUsers).FirstOrDefault(x => x.Id.Equals(httpContext.User.GetUserId()));
                    var apiFunctionRepository = (ApiFunctionRepository)httpContext.RequestServices.GetService(typeof(ApiFunctionRepository));
                    //var dbUserRoles = apiFunctionRepository.ListUserRoles(x => x.UserId.Equals(httpContext.User.GetUserId()));
                    var identityUserRoles = apiFunctionRepository.GetUserRoles(httpContext.User.GetUserId());
                    var apis = new List<ApiFunction>();
                    foreach (var item in identityUserRoles.Select(x => x.RoleId).ToList())
                    {
                        apis.AddRange(apiFunctionRepository.ListNavigation(x => !x.IsDeleted && x.ApiFunctionRoles.Any(y => y.RoleId.Equals(item))).ToList());
                    }
                    foreach (var item in existUser.ApiFunctionUsers)
                    {
                        var a = apis.FirstOrDefault(x => x.Id == item.ApiFunctionId);
                        if (a != null)
                        {
                            if (item.Status == (int)API_ROLE_STATUS.DENY)
                            {
                                apis.Remove(a);
                            }
                        }
                        else
                        {
                            if (item.Status == (int)API_ROLE_STATUS.ALLOW)
                            {
                                var b = apiFunctionRepository.List(x => !x.IsDeleted && x.Id == item.ApiFunctionId).FirstOrDefault();
                                apis.Add(new ApiFunction
                                {
                                    ActionName = b.ActionName,
                                    Id = item.ApiFunctionId,
                                    ControllerName = b.ControllerName,
                                    RelativePath = b.RelativePath,
                                    HttpMethod = b.HttpMethod,
                                });
                            }
                        }
                    }

                    var nodes = apis.GroupBy(x => x.ControllerName).Select(x => new
                    {
                        Key = x.Key,
                    });
                    if (httpContext.User.GetTotalApis() != nodes.Count())
                    {
                        httpContext.Response.StatusCode = 401; //UnAuthorized
                        await httpContext.Response.WriteAsync("401 Unauthorized");
                        return;
                    }
                    if (apis.Any(x => httpContext.Request.Path.Value.ToLower().StartsWith(Regex.Replace(("/" + x.RelativePath).ToLower(), "{(.+?)}", "")) && x.HttpMethod.Equals(httpContext.Request.Method)))
                    {
                        await _next(httpContext);
                        return;
                    }
                    else
                    {
                        httpContext.Response.StatusCode = 403; //Forbiden
                        await httpContext.Response.WriteAsync("Bạn không có quyền sử dụng chức năng này");
                    }
                }
            }
            catch (Exception ex)
            {
                await _next(httpContext);
            }
        }

        private bool IsBypassAPI(string api) => ByPassAPIsHelper.ByPassAPIs.Any(x => api.ToLower().StartsWith(x)) ? true : false;
    }
}
﻿using System.Threading.Tasks;
using HiringTutors.WebAPI.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Handler
{

    public interface IImageHandler
    {
        Task<IActionResult> UploadImage(IFormFile file, string path, string userId);
    }

    public class ImageHandler : IImageHandler
    {
        private readonly IImageWriterService _imageWriter;
        public ImageHandler(IImageWriterService imageWriter)
        {
            _imageWriter = imageWriter;
        }

        public async Task<IActionResult> UploadImage(IFormFile file, string path, string userId)
        {
            var result = await _imageWriter.UploadImage(file, path, userId);
            return new ObjectResult(result);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Helpers
{
    public static class ByPassAPIsHelper
    {
        private static string _path = Path.Combine(Directory.GetCurrentDirectory(), "Resources/Others/ByPassAPIs.txt");
        private static List<string> _apis = new List<string>();
        private static DateTime _lastModified = DateTime.Now;

        public static List<string> ByPassAPIs
        {
            get
            {
                DateTime lastModified = File.GetLastWriteTime(_path);

                if (_apis.Count == 0 || lastModified > _lastModified)
                {
                    _lastModified = lastModified;
                    _apis = File.ReadAllLines(_path).ToList();
                }

                return _apis;
            }
        }
    }
}
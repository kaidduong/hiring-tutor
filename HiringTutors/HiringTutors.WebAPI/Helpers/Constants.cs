﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Helpers
{
    public static class Roles
    {
        public const string ADMIN = "Admin";
        public const string TUTOR = "Tutor";
        public const string USER = "User";
        public const string TUTOR_NEW = "Tutor new";
    }

    public static class Policies
    {
        public const string ADMIN_OR_TUTOR = "AdminOrTutor";
    }

    public enum API_ROLE_STATUS
    {
        ALLOW = 1,
        DENY
    }

    public static class BaseURIs
    {
    }

    public enum TYPE_UPDATE
    {
        USER = 1,
        TUTOR,
        NEWS
    }

    public enum TYPE_TEACHING
    {
        ONLINE = 1,
        OFFLINE,
        BOTH
    }

    public enum TUTOR_STATUS
    {
        NEW = 1,
        WAIT_TO_VERIFY,
        INVALID,
        ACTIVE,
        DISABLE
    }

    public enum TIME_IN_DAY
    {
        ALL_DAY = 1,
        MORNING,
        AFTERNOON,
        NIGHT,
        MOR_AFTER,
        MOR_NIGHT,
        AFTER_NIGHT,
        NONE
    }

    public enum NEWS_STATUS
    {
        NEW = 1,
        WAIT_TO_VERIFY,
        INVALID,
        ACTIVE,
        DISABLE,
        HAS_CANDICATES,
        COMPLETE
    }

    public enum TUTOR_NEWS_STATUS
    {
        NEW = 1,
        ACTIVE,
        DISABLE,
        CANCEL_BY_USER,
        CANCEL_BY_TUTOR
    }

    public static class Newses
    {
        public const int DEFAULT_ADDMISION_FEE = 300;
    }
}
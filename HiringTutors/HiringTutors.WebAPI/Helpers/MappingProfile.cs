﻿using AutoMapper;
using HiringTutors.Core.Entities;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.ViewModels;

namespace Rikkonbi.WebAPI.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, UserViewModel>();
            CreateMap<ApplicationUser, UserInfoViewModel>();
            CreateMap<AddressViewModel, Address>().ReverseMap();
            CreateMap<CreateTutorView, Tutor>();
            CreateMap<Tutor, TutorViewModel>();
            CreateMap<Schedule, ScheduleViewModel>().ReverseMap();
            CreateMap<UpdateScheduleViewModel, Schedule>();
            CreateMap<CreateScheduleViewModel, Schedule>();
            CreateMap<UpdateScheduleViewModel, CreateScheduleViewModel>();
            CreateMap<CreateAddressView, Address>();
            CreateMap<UpdateAdddressView, Address>();
            CreateMap<UpdateAdddressView, CreateAddressView>();
            CreateMap<CreateCategoryView, CategotyOfSubject>().ReverseMap();
            CreateMap<CreateSubjectViewModel, Subject>().ReverseMap();
            CreateMap<InforNewsView, News>().ReverseMap();
            CreateMap<CreateInforNewsView, News>().ReverseMap();
        }
    }
}
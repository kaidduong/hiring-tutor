﻿namespace HiringTutors.WebAPI.Helpers
{
    public class JwtAuthSettings
    {
        public string Secret { get; set; }
        public int TokenTimeLifeMinutes { get; set; }
    }
}
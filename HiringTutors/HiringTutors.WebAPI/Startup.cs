﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HiringTutors.Core.Interfaces;
using HiringTutors.Infrastructure.Data.Repositories;
using HiringTutors.Infrastructure.Services;
using HiringTutors.WebAPI.Extensions;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Handler;
using HiringTutors.WebAPI.Services;
using HiringTutors.WebAPI.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;

namespace HiringTutors.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            // Initialize Serilog logger
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure DbContext
            services.ConfigureDbContext(Configuration);

            // Configure ASP.NET Identity
            services.ConfigureAspNetIdentity();

            // Configure AutoMapper
            services.ConfigureAutoMapper();

            // Configure JWT authentication
            services.ConfigureJwtAuthentication(Configuration);

            // Configure authorization
            services.ConfigureAuthorization();

            // Configure Swagger
            services.ConfigureSwagger();

            // Configure DI for application services
            services.AddScoped<ISocialAuthService, GoogleAuthService>();
            services.AddSingleton<ITokenService>(new TokenService(Configuration));

            //Configure DI for repositories
            services.AddScoped<IApiFunctionRepository, ApiFunctionRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<ITutorRepository, TutorRepository>();
            services.AddScoped<IScheduleRepository, ScheduleRepository>();
            services.AddScoped<ICategoryOfSubjectRepository, CategoryOfSubjectRepository>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();

            //Configure DI for services
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IManagerPermissionsService, ManagerPermissionsService>();
            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<ITutorService, TutorService>();
            services.AddTransient<IImageHandler, ImageHandler>();
            services.AddTransient<IImageWriterService, ImageWriterService>();
            services.AddTransient<INewsService, NewsService>();

            services.AddCors();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // In production, the Angular files will be served from this directory
            //services.AddSpaStaticFiles(configuration =>
            //{
            //    configuration.RootPath = "WebClient";
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "HiringTutors API Ver.1.0");
            });

            // Global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
#if DEBUG
            app.UseHttpsRedirection();

#endif
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Resources")),
                RequestPath = new PathString("/Resources")
            });
            app.PermissionRoleMiddleware();
            // app.UseSpaStaticFiles();
            app.UseMvc();
            //app.UseSpa(spa =>
            //{
            //    // To learn more about options for serving an Angular SPA from ASP.NET Core,
            //    // see https://go.microsoft.com/fwlink/?linkid=864501

            //    spa.Options.SourcePath = "ClientApp";
            //});
        }
    }
}
﻿using HiringTutors.WebAPI.Filters;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.Extentions
{
    public static class PermissionMiddlewareExtension
    {
        public static void PermissionRoleMiddleware(this IApplicationBuilder app)
        {
            app.UseWhen(context => context.Request.Path.StartsWithSegments("/api"), appBuilder =>
            {
                appBuilder.UseMiddleware<PermissionMiddleware>();
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Controllers
{
    public class NewsController : BaseApiController
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpPost]
        public IActionResult Create([FromBody] CreateNewsView createNewsView)
        {
            try
            {
                return Ok(_newsService.CreateNews(createNewsView, User));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetById/{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(_newsService.GetNewsById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("List")]
        public IActionResult List(int pageNum = 1, int pageSize = 15)
        {
            try
            {
                return Ok(_newsService.ListNewses(pageNum, pageSize));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetAll")]
        public IActionResult GetALL()
        {
            try
            {
                return Ok(_newsService.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetAllActivedNewses")]
        public IActionResult GetAllActivedNewses()
        {
            try
            {
                return Ok(_newsService.GetAllActivedNews());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetNewsesByUser")]
        public IActionResult GetNewsesByUser()
        {
            try
            {
                return Ok(_newsService.GetAllNewsByUser(User.GetUserId()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update(UpdateNewsView updateNewsView)
        {
            try
            {
                _newsService.UpdateNews(updateNewsView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("ConfirmNews")]
        public IActionResult ConfirmNews(ConfirmNewsView confirmNewsView)
        {
            try
            {
                _newsService.ConfirmInfoNews(confirmNewsView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("Remove/{id}")]
        public IActionResult Remove(int id)
        {
            try
            {
                _newsService.Remove(id, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("CancelCandicates")]
        public IActionResult CancelTeaching([FromBody] CancelCandicatesView cancelCandicatesView)
        {
            try
            {
                if (_newsService.CancelCandicates(cancelCandicatesView, User))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("InviteCandicate")]
        public IActionResult InviteCandicate(CandicateView candicateView)
        {
            try
            {
                if (_newsService.InviteCandicate(candicateView, User))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("ApproveCandicate")]
        public IActionResult ApproveCandicate(CandicateView candicateView)
        {
            try
            {
                if (_newsService.ApproveCandicate(candicateView, User))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
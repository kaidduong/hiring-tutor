﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Controllers
{
    public class CommonController : BaseApiController
    {
        private readonly ICommonService _commonService;

        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        [HttpPut("UpdateAddress")]
        public IActionResult UpdateAddress([FromBody] UpdateAdddressView addressView)
        {
            _commonService.UpdateAddress(addressView, User);
            return Ok();
        }

        [HttpPut("UpdateSchedule")]
        public IActionResult UpdateSchedule([FromBody] UpdateScheduleViewModel updateScheduleView)
        {
            _commonService.UpdateSchedule(updateScheduleView, User);
            return Ok();
        }

        [HttpPost("CreateCategoryOfSubject")]
        public IActionResult CreateCategoryOfSubject([FromBody] CreateCategoryView createCategoryView)
        {
            try
            {
                _commonService.CreateCategory(createCategoryView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("CreateSubject")]
        public IActionResult CreateSubject([FromBody] CreateSubjectViewModel createSubjectView)
        {
            try
            {
                _commonService.CreateSubject(createSubjectView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("GetSubjects")]
        public IActionResult GetSubjects()
        {
            try
            {
                return Ok(_commonService.GetSubjects());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
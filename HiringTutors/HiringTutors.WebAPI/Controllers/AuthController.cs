﻿using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Controllers
{
    public class AuthController : BaseApiController
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        // POST api/auth/login
        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody] LoginViewModel loginViewModel)
        {
            {
                var userView = _authService.LoginByGoogleAccount(loginViewModel);
                if (userView.Id.Equals("None"))
                {
                    return BadRequest("Tài khoản của bạn đã bị vô hiệu hóa!");
                }
                if (userView.Id == null)
                {
                    return BadRequest("The Google token is invalid or has expired!");
                }
                else
                {
                    return Ok(userView);
                }
            }
        }

        // POST api/auth/login
        [HttpPost("login-user")]
        [AllowAnonymous]
        public IActionResult LoginAccount([FromBody] LoginAccountViewModel loginViewModel)
        {
            var userView = _authService.Login(loginViewModel);
            if (userView.Id == null)
            {
                return BadRequest();
            }
            else
            {
                return Ok(userView);
            }
        }

        // POST api/auth/logout
        [HttpPost("logout")]
        public IActionResult Logout()
        {
            return Ok();
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public IActionResult Register([FromBody] AddUserView addUserView)
        {
            var userView = _authService.Register(addUserView);
            if (userView.Id == null)
            {
                return BadRequest("User account was exist!");
            }
            else
            {
                return Ok(userView);
            }
        }
    }
}
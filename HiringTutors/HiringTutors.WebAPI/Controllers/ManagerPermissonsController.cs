﻿using HiringTutors.Infrastructure.Identity;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Helpers;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HiringTutors.WebAPI.Controllers
{
    public class ManagerPermissonsController : BaseApiController
    {
        private readonly IManagerPermissionsService _managerPermissionsService;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ManagerPermissonsController(IManagerPermissionsService managerPermissionsService,
                                            UserManager<ApplicationUser> userManager,
                                            RoleManager<ApplicationRole> roleManager)
        {
            _managerPermissionsService = managerPermissionsService;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_managerPermissionsService.GetAllPermissons());
        }

        [HttpGet("List")]
        public IActionResult List(int pageNum = 1, int pageSize = 15)
        {
            return Ok(_managerPermissionsService.GetList(pageNum, pageSize));
        }

        [Authorize(Roles = Roles.ADMIN)]
        [HttpPost("MultiPost")]
        public IActionResult MultiPost()
        {
            _managerPermissionsService.MultiCreatePermission(User);
            return Ok();
        }

        //     If IgnoreApi = false then no ApiDescription objects will be created for the associated controller or action.
        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpPost]
        public IActionResult Post(ApiFunctionViewModel apiRoleViewModel)
        {
            _managerPermissionsService.Add(new ApiFunction
            {
                ActionName = apiRoleViewModel.ActionName,
                ControllerName = apiRoleViewModel.ControllerName,
                HttpMethod = apiRoleViewModel.HttpMethod,
                RelativePath = apiRoleViewModel.RelativePath,
                CreatedBy = User.Identity.Name,
                CreatedOn = DateTime.Now
            });

            return Ok();
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpDelete]
        public IActionResult Delete(ApiFunctionEditViewModel apiFunctionEditView)
        {
            if (!_managerPermissionsService.RemovePermission(apiFunctionEditView))
                return NotFound();
            return Ok();
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpGet("GetFunctionByRole/{roleName}")]
        public IActionResult GetFunctionByRole(string roleName)
        {
            var existRole = _roleManager.Roles.Include(x => x.ApiFunctionRoles).FirstOrDefault(x => x.NormalizedName.Equals(roleName.ToUpper()));
            if (existRole == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(_managerPermissionsService.GetPermissionByRole(existRole));
            }
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpGet("GetFunctionByUser")]
        public IActionResult GetFunctionByUser()
        {
            var existUser = _userManager.Users.Include(x => x.ApiFunctionUsers).FirstOrDefault(x => x.Id.Equals(User.GetUserId()));
            if (existUser == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(_managerPermissionsService.GetPermissionByUser(existUser));
            }
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpGet("GetFunctionByUserId/{userId}")]
        public IActionResult GetFunctionByUserId(string userId)
        {
            var existUser = _userManager.Users.Include(x => x.ApiFunctionUsers).FirstOrDefault(x => x.Id.Equals(userId));
            if (existUser == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(_managerPermissionsService.GetPermissionByUser(existUser));
            }
        }

        [HttpGet("GetAllRoles")]
        [Authorize(Roles = Roles.ADMIN)]
        public IActionResult GetAllRoles()
        {
            return Ok(_roleManager.Roles.ToList());
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpPost("AddFunctionToRole")]
        public IActionResult AddFunctionToRole(ApiFunctionCreateViewModel apiFunctionCreate)
        {
            if (apiFunctionCreate.ApiFunctions.Count <= 0)
            {
                return BadRequest("Danh sách api không được bỏ trống");
            }
            if (!_managerPermissionsService.AddPermissionsToRole(apiFunctionCreate))
                return NotFound();
            return Ok();
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpPost("UpdateFunctionToUser")]
        public IActionResult UpdateFunctionToUser(UpdateFunction updateFunction)
        {
            var existUser = _userManager.Users.Include(x => x.ApiFunctionUsers).FirstOrDefault(x => x.Id.Equals(updateFunction.UserId));
            if (existUser == null)
            {
                return NotFound();
            }
            else
            {
                _managerPermissionsService.UpdatePermissionToUser(updateFunction, existUser);
                return Ok();
            }
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [Authorize(Roles = Roles.ADMIN)]
        [HttpPost("UpdateManyFunctionsToUser")]
        public IActionResult UpdateManyFunctionsToUser(List<UpdateFunction> updateFunctions)
        {
            var existUser = _userManager.Users.Include(x => x.ApiFunctionUsers).FirstOrDefault(x => x.Id.Equals(updateFunctions[0].UserId));
            if (existUser == null)
            {
                return NotFound();
            }
            else
            {
                foreach (var item in updateFunctions)
                {
                    _managerPermissionsService.UpdatePermissionToUser(item, existUser);
                }
                return Ok();
            }
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [HttpPut("EditRolesOfUser")]
        [Authorize(Roles = Roles.ADMIN)]
        public IActionResult Put(EditUserRoleViewModel userRoleViewModel)
        {
            try
            {
                if (!_managerPermissionsService.EditRolesOfUser(userRoleViewModel))
                {
                    return BadRequest($"User Id ({userRoleViewModel.UserId}) không tồn tại !");
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [HttpGet("GetPermissionByGroups")]
        [Authorize(Roles = Roles.ADMIN)]
        public IActionResult GetPermissionByGroups()
        {
            try
            {
                return Ok(_managerPermissionsService.GetPermissionByGroups());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [HttpPost("CreateRole")]
        [Authorize(Roles = Roles.ADMIN)]
        public IActionResult CreateRole(CreateRoleView createRoleView)
        {
            try
            {
                _managerPermissionsService.CreateRole(createRoleView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
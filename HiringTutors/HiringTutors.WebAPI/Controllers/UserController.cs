﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService,
                                IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet("GetUserInforById")]
        public IActionResult GetUserInforById(string userId)
        {
            if (!_userService.CheckUserExist(userId))
            {
                return NotFound($"User ({userId}) is not exist.");
            }
            else
            {
                return Ok(_userService.GetUserInforById(userId));
            }
        }

        [HttpGet("GetUserInfo")]
        public IActionResult GetUserInfor()
        {
            var userId = User.GetUserId();
            if (!_userService.CheckUserExist(userId))
            {
                return NotFound($"User ({userId}) is not exist.");
            }
            else
            {
                return Ok(_userService.GetUserInforById(userId));
            }
        }

        [HttpGet("GetAllUsers")]
        public IActionResult GetAllUsers()
        {
            return Ok(_userService.GetAllUsers());
        }

        [HttpGet("ListUsers")]
        public IActionResult ListUsers(int pageNum = 1, int pageSize = 15)
        {
            return Ok(_userService.ListUsers(pageNum, pageSize));
        }

        [HttpPut("EditUserInfor")]
        public IActionResult EditUserInfor([FromBody] EditUserInfoView editUserInfoView)
        {
            var userId = User.GetUserId();
            if (!_userService.CheckUserExist(userId))
            {
                return NotFound($"User ({userId}) is not exist.");
            }
            else
            {
                _userService.EditUserInfo(editUserInfoView, User);
                return Ok();
            }
        }

        [HttpPut("ChangePassword")]
        public IActionResult EditUserInfor([FromBody] ChangePasswordRequest request)
        {
            var userId = User.GetUserId();
            if (!_userService.CheckUserExist(userId))
            {
                return NotFound($"User ({userId}) is not exist.");
            }
            else
            {
                var rs = _userService.ChangePasswordAsync(request, User);
                if (rs)
                { return Ok(); }
                else
                {
                    return BadRequest();
                }
            }
        }

        [HttpPut("RemoveUser/{userId}")]
        public IActionResult RemoveUser(string userId)
        {
            if (!_userService.CheckUserExist(userId))
            {
                return NotFound($"User ({userId}) is not exist.");
            }
            else
            {
                _userService.RemoveUser(userId, User);
                return Ok();
            }
        }
    }
}
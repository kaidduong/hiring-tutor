﻿using System.Threading.Tasks;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Handler;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Controllers
{
    public class ImageController : BaseApiController
    {
        private readonly IImageHandler _imageHandler;

        public ImageController(IImageHandler imageHandler)
        {
            _imageHandler = imageHandler;
        }

        /// <summary>
        /// Uploads an image to the server.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("uploadimage")]
        public async Task<IActionResult> UploadImage([FromForm]ImageViewModel image)
        {

            var userId = User.GetUserId();
            return await _imageHandler.UploadImage(image.File, image.Path, userId);
        }
    }
}
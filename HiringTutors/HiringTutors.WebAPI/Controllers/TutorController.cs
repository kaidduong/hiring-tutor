﻿using System;
using HiringTutors.WebAPI.Extentions;
using HiringTutors.WebAPI.Services.Interfaces;
using HiringTutors.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace HiringTutors.WebAPI.Controllers
{
    public class TutorController : BaseApiController
    {
        private readonly ITutorService _tutorService;

        public TutorController(ITutorService tutorService)
        {
            _tutorService = tutorService;
        }

        [HttpPost]
        public IActionResult Register()
        {
            try
            {
                return Ok(_tutorService.AddTutor(User));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetById/{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(_tutorService.GetById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetInforTutor()
        {
            try
            {
                return Ok(_tutorService.GetTutorInfor(User.GetUserId()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_tutorService.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("List")]
        public IActionResult List(int pageNum = 1, int pageSize = 15)
        {
            return Ok(_tutorService.ListTutors(pageNum, pageSize));
        }

        [HttpPut("Delete")]
        public IActionResult Delete(int id)
        {
            try
            {
                _tutorService.Remove(id, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update([FromBody] UpdateTutorView updateTutorView)
        {
            try
            {
                _tutorService.UpdateTutor(updateTutorView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("ConfirmTutor")]
        public IActionResult ConfirmTutor([FromBody] ConfirmTutorView confirmTutorView)
        {
            try
            {
                _tutorService.ConfirmInfoTutor(confirmTutorView, User);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("OfferToTeach/{newsId}")]
        public IActionResult OfferToTeach(int newsId)
        {
            try
            {
                if (_tutorService.OfferToTeach(newsId, User))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("CancelTeaching/{newsId}")]
        public IActionResult CancelTeaching(int newsId)
        {
            try
            {
                if (_tutorService.CancelTeaching(newsId, User))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class AddressViewModel
    {
        public int Id { get; set; }

        public string Street { get; set; }
        public string Commune { get; set; }

        public int? HouseNumber { get; set; }

        public string District { get; set; }

        public string City { get; set; }
    }

    public class CreateAddressView
    {
        public string Street { get; set; }
        public string Commune { get; set; }

        public int? HouseNumber { get; set; }

        public string District { get; set; }

        public string City { get; set; }
    }

    public class UpdateAdddressView : CreateAddressView
    {
        public int Id { get; set; }
        public int TypeUpdate { get; set; }
    }
}
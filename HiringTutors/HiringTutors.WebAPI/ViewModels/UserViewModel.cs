﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HiringTutors.WebAPI.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public IList<string> Roles { get; set; }

        // public IList<string> Apis { get; set; }
        public string FullName { get; set; }

        public string Email { get; set; }
        public string Avatar { get; set; }
        public string Token { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class EditUserRoleViewModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public IList<string> Role { get; set; }
    }

    public class AddUserView
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Phone { get; set; }
    }

    public class UserInfoView
    {
        public string FullName { get; set; }

        public string Phone { get; set; }
    }

    public class EditUserInfoView
    {
        public UserInfoView UserInfoView { get; set; }
        public bool HasUpdateInfoUser { get; set; }
        public UpdateAdddressView AddressView { get; set; }
        public bool HasUpdateAddress { get; set; }
    }

    public class ChangePasswordRequest
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }

    public class UserInfoViewModel : UserViewModel
    {
        public AddressViewModel AddressView { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class ApiFunctionViewModel
    {
        public string HttpMethod { get; set; }
        public string RelativePath { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }

    public class ApiFunctionEditViewModel
    {
        public int Id { get; set; }
    }

    public class ApiFunctionCreateViewModel
    {
        [Required]
        public string RoleId { get; set; }

        [Required]
        public List<int> ApiFunctions { get; set; }
    }

    public class ApiFunctionModel : ApiFunctionViewModel
    {
        public int ApiFunctionId { get; set; }
        public bool IsSelected { get; set; }
    }

    public class UpdateFunction
    {
        public string UserId { get; set; }
        public int ApiFunctionId { get; set; }
        public int Status { get; set; }
    }

    public class ApiDescriptionGroupsView
    {
        public string Key { get; set; }
        public IEnumerable<ApiFunctionViewModel> Items { get; set; }
    }

    public class PermissionView : ApiDescriptionGroupsView
    {
        public bool IsSelected { get; set; }
    }

    public class PermissionViewModel : ApiFunctionViewModel
    {
        public int Id { get; set; }
    }

    public class PermissionByGroupView
    {
        public string GroupName { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }

    public class CreateRoleView
    {
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class SubjectViewModel
    {
        public CategoryView CategoryView { get; set; }
        public List<SubjectView> Subjects { get; set; }
    }

    public class SubjectView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CreateSubjectViewModel
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }

    public class UpdateSubjectView : CreateSubjectViewModel
    {
        public int Id { get; set; }
    }

    public class CategoryView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CreateCategoryView
    {
        public string Name { get; set; }
    }
}
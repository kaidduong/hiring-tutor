﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string Token { get; set; }
    }

    public class LoginAccountViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
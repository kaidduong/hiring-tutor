﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        public AddressViewModel AddressView { get; set; }
        public ScheduleViewModel ScheduleView { get; set; }
        public List<SubjectView> SubjectViews { get; set; }
        public InforNewsView InforNewsView { get; set; }
    }

    public class InforNewsView : UpdateInforNewsView
    {
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ActivedOn { get; set; }
        public List<int> TutorCandicateIds { get; set; }
    }

    public class UpdateInforNewsView : CreateInforNewsView
    {
        public int AdmissionFee { get; set; }
    }

    public class CreateInforNewsView
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int TeachingType { get; set; }
        public int TuitionPerHour { get; set; }
        public string TutorRequired { get; set; }
        public string PhoneNumber { get; set; }
        public int? AmountOfTeachingTimePerLesson { get; set; }
    }

    public class CreateNewsView
    {
        public CreateAddressView CreateAddressView { get; set; }
        public CreateScheduleViewModel CreateScheduleView { get; set; }
        public List<int> SubjectIds { get; set; }
        public InforNewsView InforNewsView { get; set; }
    }

    public class UpdateNewsView
    {
        public int Id { get; set; }
        public UpdateInforNewsView UpdateInforNewsView { get; set; }
        public bool HasUpdateInfo { get; set; }
        public UpdateScheduleViewModel UpdateScheduleView { get; set; }
        public bool HasUpdateSchedule { get; set; }
        public UpdateAdddressView UpdateAddressView { get; set; }
        public bool HasUpdateAddress { get; set; }
        public List<int> SubjectIds { get; set; }
        public bool HasUpdateSubject { get; set; }
    }

    public class ConfirmNewsView
    {
        public int Id { get; set; }
        public int ConfirmedType { get; set; }
    }

    public class CancelCandicatesView
    {
        public int NewsId { get; set; }
        public List<int> TutorCandicateIds { get; set; }
    }

    public class CandicateView
    {
        public int TutorId { get; set; }
        public int NewsId { get; set; }
    }
}
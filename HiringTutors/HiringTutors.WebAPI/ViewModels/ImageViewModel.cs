﻿using Microsoft.AspNetCore.Http;

namespace HiringTutors.WebAPI.ViewModels
{
    public class ImageViewModel
    {
        public IFormFile File { get; set; }
        public string Path { get; set; }
    }

    public class ImageView
    {
        public string ImagePath { get; set; }
        public string ImageType { get; set; }
    }
}
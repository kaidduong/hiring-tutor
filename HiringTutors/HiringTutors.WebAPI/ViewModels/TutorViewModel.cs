﻿using HiringTutors.Core.Entities;
using HiringTutors.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class TutorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public string Job { get; set; }
        public int TeachingType { get; set; }
        public int TuitionPerHour { get; set; }
        public string Introduction { get; set; }
        public int Status { get; set; }
        public ScheduleViewModel ScheduleView { get; set; }
        public AddressViewModel Address { get; set; }
        public List<SubjectView> Subjects { get; set; }
        public List<ImageView> CertificationImageViews { get; set; }
    }

    public class CreateTutorView
    {
        public string UserId { get; set; }
        public string Job { get; set; } = "";
        public int TeachingType { get; set; } = (int)TYPE_TEACHING.BOTH;
        public int TuitionPerHour { get; set; } = 50;
        public int Status { get; set; } = (int)TUTOR_STATUS.NEW;
    }

    public class UpdateTutorView
    {
        public InforTutorView UpdateInforTutorView { get; set; }
        public bool HasUpdateInfo { get; set; }
        public UpdateScheduleViewModel UpdateScheduleView { get; set; }
        public bool HasUpdateSchedule { get; set; }
        public UpdateAdddressView UpdateAddressView { get; set; }
        public bool HasUpdateAddress { get; set; }
        public List<int> SubjectIds { get; set; }
        public bool HasUpdateSubject { get; set; }
    }

    public class InforTutorView
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Job { get; set; }
        public int TeachingType { get; set; }
        public int TuitionPerHour { get; set; }
        public string Introduction { get; set; }
    }

    public class TutorView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Job { get; set; }
        public int TeachingType { get; set; }
        public int TuitionPerHour { get; set; }
        public string Introduction { get; set; }
        public Address Address { get; set; }
        public List<Subject> Subjects { get; set; }
    }

    public class ConfirmTutorView
    {
        public int Id { get; set; }
        public int ConfirmedType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HiringTutors.WebAPI.ViewModels
{
    public class ScheduleViewModel : CreateScheduleViewModel
    {
        public int Id { get; set; }
    }

    public class CreateScheduleViewModel
    {
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int Sunday { get; set; }
    }

    public class UpdateScheduleViewModel : ScheduleViewModel
    {
        public int TypeUpdate { get; set; }
    }
}
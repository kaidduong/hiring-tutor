﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface ITutorRepository : IRepository<Tutor>, IRepositoryAsync<Tutor>
    {
        IQueryable<Tutor> ListNavi(Expression<Func<Tutor, bool>> predicate);
    }

    public class TutorRepository : EfRepository<Tutor>, ITutorRepository
    {
        public TutorRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Tutor> ListNavi(Expression<Func<Tutor, bool>> predicate)
        {
            return _dbContext.Set<Tutor>().Include(t => t.Schedule).Include(t => t.TutorNews).Include(t => t.TutorSubjects).Include(t => t.CertificationImages).Where(predicate);
        }
    }
}
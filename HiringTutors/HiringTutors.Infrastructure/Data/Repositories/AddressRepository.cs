﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Interfaces;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface IAddressRepository : IRepository<Address>, IRepositoryAsync<Address>
    {
    }

    public class AddressRepository : EfRepository<Address>, IAddressRepository
    {
        public AddressRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }
    }
}
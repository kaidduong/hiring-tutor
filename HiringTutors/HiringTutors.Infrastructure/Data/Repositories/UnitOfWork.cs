﻿using HiringTutors.Core.Interfaces;
using Rikkonbi.Infrastructure.Data;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private HiringTutorsDbContext _dbContext;

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
﻿using HiringTutors.Core.Interfaces;
using HiringTutors.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface IApiFunctionRepository : IRepository<ApiFunction>, IRepositoryAsync<ApiFunction>
    {
        IQueryable<ApiFunction> ListNavigation(Expression<Func<ApiFunction, bool>> predicate);

        IQueryable<ApplicationRole> PermissionsByGroups(Expression<Func<ApplicationRole, bool>> predicate);

        List<IdentityUserRole<string>> GetUserRoles(string userId);

        List<ApplicationRole> GetRoles(string userId);
    }

    public class ApiFunctionRepository : EfRepository<ApiFunction>, IApiFunctionRepository
    {
        public ApiFunctionRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<ApiFunction> ListNavigation(Expression<Func<ApiFunction, bool>> predicate)
        {
            return _dbContext.Set<ApiFunction>().Include(x => x.ApiFunctionRoles).Where(predicate);
        }

        public List<IdentityUserRole<string>> GetUserRoles(string userId)
        {
            return _dbContext.UserRoles.Where(x => x.UserId.Equals(userId)).ToList();
        }

        public List<ApplicationRole> GetRoles(string userId)
        {
            var roleIds = _dbContext.UserRoles.Where(x => x.UserId.Equals(userId)).Select(x => x.RoleId).ToList();
            return _dbContext.ApplicationRoles.Include(x => x.ApiFunctionRoles).Where(x => roleIds.Contains(x.Id)).ToList();
        }

        public IQueryable<ApplicationRole> PermissionsByGroups(Expression<Func<ApplicationRole, bool>> predicate)
        {
            return _dbContext.Set<ApplicationRole>().Include(x => x.ApiFunctionRoles).Where(predicate);
        }
    }
}
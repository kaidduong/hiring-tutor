﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface ISubjectRepository : IRepository<Subject>, IRepositoryAsync<Subject>
    {
        IQueryable<Subject> ListNavi(Expression<Func<Subject, bool>> predicate);
    }

    public class SubjectRepository : EfRepository<Subject>, ISubjectRepository
    {
        public SubjectRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Subject> ListNavi(Expression<Func<Subject, bool>> predicate)
        {
            return _dbContext.Set<Subject>().Include(t => t.TutorSubjects).Include(t => t.NewsSubjects).Where(predicate);
        }
    }
}
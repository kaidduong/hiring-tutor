﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Interfaces;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface IScheduleRepository : IRepository<Schedule>, IRepositoryAsync<Schedule>
    {
    }

    public class ScheduleRepository : EfRepository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }
    }
}
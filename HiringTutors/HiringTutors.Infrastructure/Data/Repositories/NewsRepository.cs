﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface INewsRepository : IRepository<News>, IRepositoryAsync<News>
    {
        IQueryable<News> ListNavi(Expression<Func<News, bool>> predicate);
    }

    public class NewsRepository : EfRepository<News>, INewsRepository
    {
        public NewsRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<News> ListNavi(Expression<Func<News, bool>> predicate)
        {
            return _dbContext.Set<News>().Include(t => t.Schedule).Include(t => t.TutorNews).Include(t => t.NewsSubjects).Include(t => t.Address).Where(predicate);
        }
    }
}
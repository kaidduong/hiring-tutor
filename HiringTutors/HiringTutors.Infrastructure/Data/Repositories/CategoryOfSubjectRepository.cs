﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Rikkonbi.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HiringTutors.Infrastructure.Data.Repositories
{
    public interface ICategoryOfSubjectRepository : IRepository<CategotyOfSubject>, IRepositoryAsync<CategotyOfSubject>
    {
        IQueryable<CategotyOfSubject> ListNavi(Expression<Func<CategotyOfSubject, bool>> predicate);
    }

    public class CategoryOfSubjectRepository : EfRepository<CategotyOfSubject>, ICategoryOfSubjectRepository
    {
        public CategoryOfSubjectRepository(HiringTutorsDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<CategotyOfSubject> ListNavi(Expression<Func<CategotyOfSubject, bool>> predicate)
        {
            return _dbContext.Set<CategotyOfSubject>().Include(x => x.Subjects).Where(predicate);
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using HiringTutors.Infrastructure.Identity;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class ApiFunctionUserConfiguration : IEntityTypeConfiguration<ApiFunctionUser>
    {
        public void Configure(EntityTypeBuilder<ApiFunctionUser> builder)
        {
            builder.HasKey(au => new { au.ApiFunctionId, au.UserId });
            builder.Property(au => au.Status).IsRequired(true);
        }
    }
}
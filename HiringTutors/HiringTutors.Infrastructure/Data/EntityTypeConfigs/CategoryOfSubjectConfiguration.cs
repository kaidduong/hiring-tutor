﻿using HiringTutors.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class CategoryOfSubjectConfiguration : IEntityTypeConfiguration<CategotyOfSubject>
    {
        public void Configure(EntityTypeBuilder<CategotyOfSubject> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name)
                    .HasMaxLength(100)
                    .IsRequired();
            builder.Property(c => c.IsDeleted)
                     .IsRequired()
                    .HasDefaultValue(false);
            builder.Property(c => c.CreatedOn)
                .IsRequired();

            builder.Property(c => c.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(c => c.UpdatedOn)
                .IsRequired(false);
            builder.Property(c => c.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);

            builder.HasData(
                new CategotyOfSubject
                {
                    Id = 1,
                    Name = "Khoa Học Tự Nhiên",
                    CreatedBy = "AUTO",
                    CreatedOn = DateTime.Now
                },
                new CategotyOfSubject
                {
                    Id = 2,
                    Name = "Khoa Học Xã Hội",
                    CreatedBy = "AUTO",
                    CreatedOn = DateTime.Now
                },
                new CategotyOfSubject
                {
                    Id = 3,
                    Name = "Năng Khiếu",
                    CreatedBy = "AUTO",
                    CreatedOn = DateTime.Now
                }
                );
        }
    }
}
﻿using HiringTutors.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.HouseNumber)
                      .IsRequired(false);
            builder.Property(s => s.Street)
                    .HasMaxLength(100)
                      .IsRequired(false);
            builder.Property(s => s.District)
                    .HasMaxLength(100)
                      .IsRequired();
            builder.Property(s => s.City)
                    .HasMaxLength(100)
                      .IsRequired();
            builder.Property(s => s.Commune)
                    .HasMaxLength(100)
                      .IsRequired();
            builder.Property(s => s.Street)
                    .HasMaxLength(100)
                      .IsRequired(false);
            builder.Property(s => s.IsDeleted)
                      .IsRequired()
                     .HasDefaultValue(false);
            builder.Property(s => s.CreatedOn)
                .IsRequired();

            builder.Property(s => s.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(s => s.UpdatedOn)
                .IsRequired(false);
            builder.Property(s => s.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);
        }
    }
}
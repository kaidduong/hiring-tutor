﻿using HiringTutors.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class ScheduleConfiguration : IEntityTypeConfiguration<Schedule>
    {
        public void Configure(EntityTypeBuilder<Schedule> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.Monday)
                .IsRequired();
            builder.Property(s => s.Tuesday)
                .IsRequired();
            builder.Property(s => s.Wednesday)
                .IsRequired();
            builder.Property(s => s.Thursday)
                .IsRequired();
            builder.Property(s => s.Friday)
                .IsRequired();
            builder.Property(s => s.Saturday)
                .IsRequired();
            builder.Property(s => s.Sunday)
                .IsRequired();

            builder.Property(s => s.IsDeleted)
                     .IsRequired()
                    .HasDefaultValue(false);
            builder.Property(s => s.CreatedOn)
                .IsRequired();

            builder.Property(s => s.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(s => s.UpdatedOn)
                .IsRequired(false);
            builder.Property(s => s.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);
        }
    }
}
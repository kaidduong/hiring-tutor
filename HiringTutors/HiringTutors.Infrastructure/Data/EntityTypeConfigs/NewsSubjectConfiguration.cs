﻿using HiringTutors.Core.Entities.EntityRelationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class NewsSubjectConfiguration : IEntityTypeConfiguration<NewsSubject>
    {
        public void Configure(EntityTypeBuilder<NewsSubject> builder)
        {
            builder.HasKey(ar => new { ar.NewsId, ar.SubjectId });
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using HiringTutors.Infrastructure.Identity;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class ApiFunctionRoleConfiguration : IEntityTypeConfiguration<ApiFunctionRole>
    {
        public void Configure(EntityTypeBuilder<ApiFunctionRole> builder)
        {
            builder.HasKey(ar => new { ar.ApiFunctionId, ar.RoleId });
        }
    }
}
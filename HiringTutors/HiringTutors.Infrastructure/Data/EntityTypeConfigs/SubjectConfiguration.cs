﻿using HiringTutors.Core.Entities;
using HiringTutors.Core.Entities.EntityRelationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class SubjectConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.CategoryId)
                      .IsRequired();
            builder.Property(s => s.Name)
                    .HasMaxLength(100)
                      .IsRequired();
            builder.Property(s => s.IsDeleted)
                      .IsRequired()
                     .HasDefaultValue(false);
            builder.Property(s => s.CreatedOn)
                .IsRequired();

            builder.Property(s => s.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(s => s.UpdatedOn)
                .IsRequired(false);
            builder.Property(s => s.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);

            // Category of subject has many subjects
            builder.HasOne(s => s.Categoty)
               .WithMany(c => c.Subjects)
               .HasForeignKey(s => s.CategoryId);
            // Each Sujbject has many NewsSubjects
            builder.HasMany(s => s.NewsSubjects)
                    .WithOne(ns => ns.Subject)
                    .HasForeignKey(ns => ns.SubjectId);

            // Each Subject has many TutorSubjects
            builder.HasMany(s => s.TutorSubjects)
                    .WithOne(ts => ts.Subject)
                    .HasForeignKey(ts => ts.SubjectId);
        }
    }
}
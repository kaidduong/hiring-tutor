﻿using HiringTutors.Core.Entities.EntityRelationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class TutorSubjectConfiguration : IEntityTypeConfiguration<TutorSubject>
    {
        public void Configure(EntityTypeBuilder<TutorSubject> builder)
        {
            builder.HasKey(ar => new { ar.TutorId, ar.SubjectId });
        }
    }
}
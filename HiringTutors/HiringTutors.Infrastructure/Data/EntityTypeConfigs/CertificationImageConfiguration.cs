﻿using HiringTutors.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class CertificationImageConfiguration : IEntityTypeConfiguration<CertificationImage>
    {
        public void Configure(EntityTypeBuilder<CertificationImage> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name)
                    .HasMaxLength(100)
                      .IsRequired();
            builder.Property(c => c.TutorId)
                      .IsRequired();
            builder.Property(c => c.IsDeleted)
                      .IsRequired()
                     .HasDefaultValue(false);
            builder.Property(c => c.CreatedOn)
                .IsRequired();

            builder.Property(c => c.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(c => c.UpdatedOn)
                .IsRequired(false);
            builder.Property(c => c.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);

            // Each CertificationImage has only one Tutor
            builder.HasOne(c => c.Tutor)
                    .WithMany(t => t.CertificationImages)
                    .HasForeignKey(c => c.TutorId);
        }
    }
}
﻿using HiringTutors.Core.Entities.EntityRelationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class TutorNewsConfiguration : IEntityTypeConfiguration<TutorNews>
    {
        public void Configure(EntityTypeBuilder<TutorNews> builder)
        {
            builder.HasKey(ar => new { ar.Id, ar.TutorId, ar.NewsId });
            builder.Property(n => n.StartedOn)
                    .IsRequired(false);
            builder.Property(n => n.Status)
                    .IsRequired()
                    .HasDefaultValue(1); // New
            builder.Property(n => n.IsPurcharseFee)
                    .IsRequired()
                    .HasDefaultValue(false);
            builder.Property(s => s.UpdatedOn)
                 .IsRequired(false);
            builder.Property(s => s.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);
        }
    }
}
﻿using HiringTutors.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class NewsConfiguration : IEntityTypeConfiguration<News>
    {
        public void Configure(EntityTypeBuilder<News> builder)
        {
            builder.HasKey(n => n.Id);
            builder.Property(n => n.UserId)
                    .HasMaxLength(50)
                    .IsRequired();
            builder.Property(n => n.ScheduleId)
                    .IsRequired();
            builder.Property(n => n.AddressId)
                    .IsRequired();
            builder.Property(n => n.AdmissionFee)
                    .IsRequired();
            builder.Property(n => n.TuitionPerHour)
                    .IsRequired();
            builder.Property(n => n.Title)
                    .HasMaxLength(500)
                    .IsRequired();
            builder.Property(n => n.Content)
                    .HasMaxLength(1000)
                    .IsRequired();
            builder.Property(n => n.PhoneNumber)
                   .HasMaxLength(100)
                   .IsRequired(false);
            builder.Property(n => n.AmountOfTeachingTimePerLesson)
                   .IsRequired(false);
            builder.Property(n => n.TeachingType)
                    .IsRequired()
                    .HasDefaultValue(1); // 1 = OFFLINE
            builder.Property(n => n.TutorRequired)
                    .HasMaxLength(200)
                    .IsRequired(false);
            builder.Property(n => n.Status)
                    .IsRequired()
                    .HasDefaultValue(1);// 1 = New

            builder.Property(n => n.IsDeleted)
                          .IsRequired()
                         .HasDefaultValue(false);
            builder.Property(n => n.CreatedOn)
                    .IsRequired();

            builder.Property(n => n.CreatedBy)
                    .HasMaxLength(100)
                    .IsRequired();
            builder.Property(n => n.UpdatedOn)
                    .IsRequired(false);
            builder.Property(n => n.UpdatedOn)
                    .IsRequired(false);
            builder.Property(n => n.UpdatedBy)
                    .HasMaxLength(100)
                    .IsRequired(false);

            // Each News has many NewsSubjects
            builder.HasMany(s => s.NewsSubjects)
                    .WithOne(ts => ts.News)
                    .HasForeignKey(ts => ts.NewsId);
            // Each News has many TutorNews
            builder.HasMany(s => s.TutorNews)
                    .WithOne(ts => ts.News)
                    .HasForeignKey(ts => ts.NewsId);

            // News has only one address
            builder.HasOne(n => n.Address)
                    .WithOne(a => a.News);
            // News has only one schedule
            builder.HasOne(n => n.Schedule)
                    .WithOne(a => a.News);
        }
    }
}
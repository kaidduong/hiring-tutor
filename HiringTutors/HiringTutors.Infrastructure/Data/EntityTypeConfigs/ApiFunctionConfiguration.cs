﻿using HiringTutors.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class ApiFunctionConfiguration : IEntityTypeConfiguration<ApiFunction>
    {
        public void Configure(EntityTypeBuilder<ApiFunction> builder)
        {
            builder.HasKey(api => api.Id);
            builder.Property(api => api.HttpMethod)
                    .HasMaxLength(50)
                    .IsRequired();
            builder.Property(api => api.RelativePath)
                    .HasMaxLength(300)
                    .IsRequired();
            builder.Property(api => api.IsDeleted)
                    .HasDefaultValue(false)
                    .IsRequired();
            builder.Property(api => api.ActionName)
                    .HasMaxLength(50)
                    .IsRequired();
            builder.Property(api => api.ControllerName)
                    .HasMaxLength(50)
                    .IsRequired();
            builder.Property(r => r.CreatedOn)
                .IsRequired();

            builder.Property(r => r.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(r => r.UpdatedOn)
                .IsRequired(false);

            builder.Property(r => r.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);
            // Each ApiFunction can have many entries in the ApFunctionRole join table
            builder.HasMany(r => r.ApiFunctionRoles)
                    .WithOne(ur => ur.ApiFunction)
                    .HasForeignKey(ur => ur.ApiFunctionId)
                    .IsRequired();
        }
    }
}
﻿using HiringTutors.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Infrastructure.Data.EntityTypeConfigs
{
    public class TutorConfiguration : IEntityTypeConfiguration<Tutor>
    {
        public void Configure(EntityTypeBuilder<Tutor> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.UserId)
                   .HasMaxLength(50)
                     .IsRequired();
            builder.Property(s => s.ScheduleId)
                      .IsRequired(false);
            builder.Property(s => s.Job)
                    .HasMaxLength(100)
                      .IsRequired();
            builder.Property(s => s.Introduction)
                    .HasMaxLength(500)
                      .IsRequired(false);
            builder.Property(s => s.TeachingType)
                      .IsRequired();
            builder.Property(s => s.Status)
                      .IsRequired();

            builder.Property(s => s.IsDeleted)
                      .IsRequired()
                     .HasDefaultValue(false);
            builder.Property(s => s.CreatedOn)
                .IsRequired();

            builder.Property(s => s.CreatedBy)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(s => s.UpdatedOn)
                .IsRequired(false);
            builder.Property(s => s.UpdatedBy)
                .HasMaxLength(100)
                .IsRequired(false);

            // Each Tutor has many TutorSubjects
            builder.HasMany(s => s.TutorSubjects)
                    .WithOne(ts => ts.Tutor)
                    .HasForeignKey(ts => ts.TutorId);

            // Each Tutor has many TutorNews
            builder.HasMany(s => s.TutorNews)
                    .WithOne(ts => ts.Tutor)
                    .HasForeignKey(ts => ts.TutorId);

            // Tutor has only one schedule
            builder.HasOne(n => n.Schedule)
                    .WithOne(a => a.Tutor);
        }
    }
}
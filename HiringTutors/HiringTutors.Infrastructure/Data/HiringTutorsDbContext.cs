﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using HiringTutors.Infrastructure.Data.EntityTypeConfigs;
using HiringTutors.Infrastructure.Identity;
using HiringTutors.Core.Entities;
using HiringTutors.Core.Entities.EntityRelationships;

namespace Rikkonbi.Infrastructure.Data
{
    public class HiringTutorsDbContext : IdentityDbContext
    {
        public HiringTutorsDbContext(DbContextOptions<HiringTutorsDbContext> options) : base(options)
        { }

        public DbSet<ApiFunction> ApiFunction { get; set; }
        public DbSet<ApiFunctionRole> ApiFunctionRoles { get; set; }
        public DbSet<ApiFunctionUser> ApiFunctionUsers { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<CategotyOfSubject> CategotyOfSubjects { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Tutor> Tutors { get; set; }
        public DbSet<News> Newses { get; set; }
        public DbSet<NewsSubject> NewsSubjects { get; set; }
        public DbSet<TutorSubject> TutorSubjects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new ApplicationRoleConfiguration());
            builder.ApplyConfiguration(new ApplicationUserConfiguration());
            builder.ApplyConfiguration(new ApiFunctionConfiguration());
            builder.ApplyConfiguration(new ApiFunctionRoleConfiguration());
            builder.ApplyConfiguration(new ApiFunctionUserConfiguration());
            builder.ApplyConfiguration(new CategoryOfSubjectConfiguration());
            builder.ApplyConfiguration(new SubjectConfiguration());
            builder.ApplyConfiguration(new AddressConfiguration());
            builder.ApplyConfiguration(new ScheduleConfiguration());
            builder.ApplyConfiguration(new TutorConfiguration());
            builder.ApplyConfiguration(new NewsConfiguration());
            builder.ApplyConfiguration(new TutorSubjectConfiguration());
            builder.ApplyConfiguration(new NewsSubjectConfiguration());
        }
    }
}
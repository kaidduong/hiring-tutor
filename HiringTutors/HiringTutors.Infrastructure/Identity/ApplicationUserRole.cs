﻿using HiringTutors.Core.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace HiringTutors.Infrastructure.Identity
{
    public class ApplicationUserRole : IdentityUserRole<string>
    {
        public virtual ApplicationUser User { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
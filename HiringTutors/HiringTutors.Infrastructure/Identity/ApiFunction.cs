﻿using HiringTutors.Core.Interfaces;
using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;

namespace HiringTutors.Infrastructure.Identity
{
    public class ApiFunction : BaseEntity, IAggregateRoot, IAuditable
    {
        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }
        public string HttpMethod { get; set; }
        public string ControllerName { get; set; }
        public string RelativePath { get; set; }
        public string ActionName { get; set; }
        public virtual ICollection<ApiFunctionRole> ApiFunctionRoles { get; set; }
    }
}
﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace HiringTutors.Infrastructure.Identity
{
    public static class IdentityDataInitializer
    {
        public static void SeedData(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        private static void SeedRoles(RoleManager<ApplicationRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                var role = new ApplicationRole
                {
                    Name = "Admin",
                    Description = "Perform all the operations.",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "Initializer"
                };
                roleManager.CreateAsync(role).Wait();
            }

            if (!roleManager.RoleExistsAsync("User").Result)
            {
                var role = new ApplicationRole
                {
                    Name = "User",
                    Description = "Perform normal operations.",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "Initializer"
                };
                roleManager.CreateAsync(role).Wait();
            }

            if (!roleManager.RoleExistsAsync("Tutor").Result)
            {
                var role = new ApplicationRole
                {
                    Name = "Tutor",
                    Description = "Perform tutor operations.",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "Initializer"
                };
                roleManager.CreateAsync(role).Wait();
            }
        }

        private static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByEmailAsync("kaidduong@gmail.com").Result == null)
            {
                var user = new ApplicationUser
                {
                    UserName = "kaidduong",
                    Email = "kaidduong@gmail.com",
                    FullName = "Dương Quỳnh Quang",
                    Avatar = "https://genk.mediacdn.vn/2019/2/23/cat1-15508927539751808160147.jpg",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "Initializer"
                };

                IdentityResult result = userManager.CreateAsync(user, "Default@password123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        }
    }
}
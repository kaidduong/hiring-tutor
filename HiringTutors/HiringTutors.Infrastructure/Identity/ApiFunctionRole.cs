﻿namespace HiringTutors.Infrastructure.Identity
{
    public class ApiFunctionRole
    {
        public string RoleId { get; set; }
        public int ApiFunctionId { get; set; }
        public virtual ApiFunction ApiFunction { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
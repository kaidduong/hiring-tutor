﻿namespace HiringTutors.Infrastructure.Identity
{
    public class ApiFunctionUser
    {
        public string UserId { get; set; }
        public int ApiFunctionId { get; set; }
        public int Status { get; set; }
        public virtual ApiFunction ApiFunction { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
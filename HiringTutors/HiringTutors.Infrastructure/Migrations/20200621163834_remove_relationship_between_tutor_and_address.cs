﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class remove_relationship_between_tutor_and_address : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors");

            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "Tutors");

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 38, 33, 386, DateTimeKind.Local).AddTicks(2720));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 38, 33, 388, DateTimeKind.Local).AddTicks(9155));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 38, 33, 388, DateTimeKind.Local).AddTicks(9170));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "Tutors",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 5, 1, 905, DateTimeKind.Local).AddTicks(7520));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 5, 1, 907, DateTimeKind.Local).AddTicks(3407));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 5, 1, 907, DateTimeKind.Local).AddTicks(3428));

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors",
                column: "AddressId",
                unique: true,
                filter: "[AddressId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

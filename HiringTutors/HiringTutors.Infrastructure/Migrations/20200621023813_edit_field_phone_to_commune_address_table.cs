﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class edit_field_phone_to_commune_address_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors");

            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors");

            migrationBuilder.RenameColumn(
                name: "Phone",
                table: "Addresses",
                newName: "Commune");

            migrationBuilder.AlterColumn<int>(
                name: "ScheduleId",
                table: "Tutors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "Tutors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 9, 38, 13, 3, DateTimeKind.Local).AddTicks(5505));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 9, 38, 13, 4, DateTimeKind.Local).AddTicks(7954));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 9, 38, 13, 4, DateTimeKind.Local).AddTicks(7972));

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors",
                column: "AddressId",
                unique: true,
                filter: "[AddressId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                unique: true,
                filter: "[ScheduleId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                principalTable: "Schedules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors");

            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors");

            migrationBuilder.RenameColumn(
                name: "Commune",
                table: "Addresses",
                newName: "Phone");

            migrationBuilder.AlterColumn<int>(
                name: "ScheduleId",
                table: "Tutors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "Tutors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 15, 10, 54, 39, 343, DateTimeKind.Local).AddTicks(9044));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 15, 10, 54, 39, 345, DateTimeKind.Local).AddTicks(4062));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 15, 10, 54, 39, 345, DateTimeKind.Local).AddTicks(4078));

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors",
                column: "AddressId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                principalTable: "Schedules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

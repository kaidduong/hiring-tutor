﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class add_relationship_between_Tutor_and_News : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TutorNews",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TutorId = table.Column<int>(nullable: false),
                    NewsId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    StartedOn = table.Column<DateTime>(nullable: true),
                    IsPurcharseFee = table.Column<bool>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TutorNews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TutorNews_Newses_NewsId",
                        column: x => x.NewsId,
                        principalTable: "Newses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TutorNews_Tutors_TutorId",
                        column: x => x.TutorId,
                        principalTable: "Tutors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 26, 0, 11, 6, 123, DateTimeKind.Local).AddTicks(3097));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 26, 0, 11, 6, 125, DateTimeKind.Local).AddTicks(3948));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 26, 0, 11, 6, 125, DateTimeKind.Local).AddTicks(3964));

            migrationBuilder.CreateIndex(
                name: "IX_TutorNews_NewsId",
                table: "TutorNews",
                column: "NewsId");

            migrationBuilder.CreateIndex(
                name: "IX_TutorNews_TutorId",
                table: "TutorNews",
                column: "TutorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TutorNews");

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 24, 20, 10, 19, 891, DateTimeKind.Local).AddTicks(7410));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 24, 20, 10, 19, 892, DateTimeKind.Local).AddTicks(9092));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 24, 20, 10, 19, 892, DateTimeKind.Local).AddTicks(9106));
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class add_phone_and_time_per_lesson_for_newses_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TeachingType",
                table: "Newses",
                nullable: false,
                defaultValue: 1,
                oldClrType: typeof(int),
                oldDefaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "Newses",
                nullable: false,
                defaultValue: 1,
                oldClrType: typeof(int),
                oldDefaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AmountOfTeachingTimePerLesson",
                table: "Newses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Newses",
                maxLength: 100,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 24, 7, 31, 49, 63, DateTimeKind.Local).AddTicks(6762));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 24, 7, 31, 49, 65, DateTimeKind.Local).AddTicks(2323));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 24, 7, 31, 49, 65, DateTimeKind.Local).AddTicks(2344));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmountOfTeachingTimePerLesson",
                table: "Newses");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Newses");

            migrationBuilder.AlterColumn<int>(
                name: "TeachingType",
                table: "Newses",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldDefaultValue: 1);

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "Newses",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldDefaultValue: 1);

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 38, 33, 386, DateTimeKind.Local).AddTicks(2720));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 38, 33, 388, DateTimeKind.Local).AddTicks(9155));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 38, 33, 388, DateTimeKind.Local).AddTicks(9170));
        }
    }
}

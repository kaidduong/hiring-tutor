﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class edit_Tutor_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors");

            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors");

            migrationBuilder.AlterColumn<int>(
                name: "ScheduleId",
                table: "Tutors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "Tutors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 16, 6, 14, 30, 715, DateTimeKind.Local).AddTicks(634));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 16, 6, 14, 30, 716, DateTimeKind.Local).AddTicks(2348));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 16, 6, 14, 30, 716, DateTimeKind.Local).AddTicks(2359));

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors",
                column: "AddressId",
                unique: true,
                filter: "[AddressId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                unique: true,
                filter: "[ScheduleId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                principalTable: "Schedules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors");

            migrationBuilder.DropForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors");

            migrationBuilder.DropIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors");

            migrationBuilder.AlterColumn<int>(
                name: "ScheduleId",
                table: "Tutors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "Tutors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 13, 8, 38, 51, 758, DateTimeKind.Local).AddTicks(6045));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 13, 8, 38, 51, 761, DateTimeKind.Local).AddTicks(2365));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 13, 8, 38, 51, 761, DateTimeKind.Local).AddTicks(2377));

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_AddressId",
                table: "Tutors",
                column: "AddressId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tutors_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Addresses_AddressId",
                table: "Tutors",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tutors_Schedules_ScheduleId",
                table: "Tutors",
                column: "ScheduleId",
                principalTable: "Schedules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

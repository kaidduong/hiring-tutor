﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class adding_addressId_field_for_AplicationUser_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "AspNetUsers");
        }
    }
}
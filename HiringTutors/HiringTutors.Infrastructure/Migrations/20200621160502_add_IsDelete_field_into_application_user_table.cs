﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HiringTutors.Infrastructure.Migrations
{
    public partial class add_IsDelete_field_into_application_user_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AspNetUsers",
                nullable: true,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 5, 1, 905, DateTimeKind.Local).AddTicks(7520));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 5, 1, 907, DateTimeKind.Local).AddTicks(3407));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 23, 5, 1, 907, DateTimeKind.Local).AddTicks(3428));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 9, 38, 13, 3, DateTimeKind.Local).AddTicks(5505));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 9, 38, 13, 4, DateTimeKind.Local).AddTicks(7954));

            migrationBuilder.UpdateData(
                table: "CategotyOfSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2020, 6, 21, 9, 38, 13, 4, DateTimeKind.Local).AddTicks(7972));
        }
    }
}

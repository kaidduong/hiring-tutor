﻿using Google.Apis.Auth;
using HiringTutors.Core.Entities;
using HiringTutors.Core.Extensions;
using HiringTutors.Core.Interfaces;

namespace HiringTutors.Infrastructure.Services
{
    public class GoogleAuthService : ISocialAuthService
    {
        private GoogleJsonWebSignature.Payload _jwtPayload;
        public bool IsAuthenticated { get; set; }

        public void ValidateToken(string token)
        {
            Guard.Against.NullOrWhiteSpace(token, nameof(token));

            try
            {
                _jwtPayload = GoogleJsonWebSignature.ValidateAsync(token).Result;
                IsAuthenticated = true;
            }
            catch
            {
                IsAuthenticated = false;
            }
        }

        public ISocialUserProfile GetUserProfile()
        {
            var userProfile = new SocialUserProfile
            {
                Name = _jwtPayload.Name,
                Email = _jwtPayload.Email,
                Avatar = _jwtPayload.Picture
            };

            return userProfile;
        }
    }
}
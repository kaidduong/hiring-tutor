﻿using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HiringTutors.Core.Interfaces
{
    public interface IRepository<T> where T : BaseEntity, IAggregateRoot
    {
        T GetById(int id);

        List<T> ListAll();

        List<T> List(ISpecification<T> spec);

        IQueryable<T> List(Expression<Func<T, bool>> predicate);

        T Add(T entity);

        T Update(T entity);

        T Delete(T entity);

        T UpdateNotSave(T entity);

        T AddNotSave(T entity);

        T DeleteNotSave(T entity);
    }
}
﻿namespace HiringTutors.Core.Interfaces
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
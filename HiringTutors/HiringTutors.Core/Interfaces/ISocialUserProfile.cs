﻿namespace HiringTutors.Core.Interfaces
{
    public interface ISocialUserProfile
    {
        string Name { get; set; }
        string Email { get; set; }
        string Avatar { get; set; }
    }
}
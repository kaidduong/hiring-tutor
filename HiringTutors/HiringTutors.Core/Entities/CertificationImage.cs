﻿using HiringTutors.Core.Interfaces;
using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities
{
    public class CertificationImage : BaseEntity, IAggregateRoot, IAuditable
    {
        public string Image { get; set; }
        public string Name { get; set; }
        public int TutorId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public virtual Tutor Tutor { get; set; }
    }
}
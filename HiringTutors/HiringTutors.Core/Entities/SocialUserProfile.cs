﻿using HiringTutors.Core.Interfaces;

namespace HiringTutors.Core.Entities
{
    public class SocialUserProfile : ISocialUserProfile
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
    }
}
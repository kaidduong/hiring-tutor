﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities.EntityRelationships
{
    public class NewsSubject
    {
        public int NewsId { get; set; }
        public int SubjectId { get; set; }
        public virtual News News { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
﻿using HiringTutors.Core.Interfaces;
using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities.EntityRelationships
{
    public class TutorNews : BaseEntity, IAggregateRoot
    {
        public int TutorId { get; set; }
        public int NewsId { get; set; }
        public int Status { get; set; } // New, Disable, Active, Cancel
        public DateTime? StartedOn { get; set; }
        public bool IsPurcharseFee { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public virtual Tutor Tutor { get; set; }
        public virtual News News { get; set; }
    }
}
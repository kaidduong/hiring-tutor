﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities.EntityRelationships
{
    public class TutorSubject
    {
        public int TutorId { get; set; }
        public int SubjectId { get; set; }
        public virtual Tutor Tutor { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
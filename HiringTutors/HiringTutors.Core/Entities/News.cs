﻿using HiringTutors.Core.Entities.EntityRelationships;
using HiringTutors.Core.Interfaces;
using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities
{
    public class News : BaseEntity, IAggregateRoot, IAuditable
    {
        public string UserId { get; set; }
        public int AddressId { get; set; }
        public int ScheduleId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int TeachingType { get; set; }
        public int TuitionPerHour { get; set; }
        public int AdmissionFee { get; set; }
        public string TutorRequired { get; set; }
        public string PhoneNumber { get; set; }
        public int? AmountOfTeachingTimePerLesson { get; set; }
        public int Status { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? ActivedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public virtual Schedule Schedule { get; set; }
        public virtual Address Address { get; set; }
        public virtual ICollection<NewsSubject> NewsSubjects { get; set; }
        public virtual ICollection<TutorNews> TutorNews { get; set; }
    }
}
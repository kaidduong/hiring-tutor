﻿using HiringTutors.Core.Interfaces;
using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities
{
    public class Address : BaseEntity, IAggregateRoot, IAuditable
    {
        public string Street { get; set; }
        public string Commune { get; set; }
        public int? HouseNumber { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public virtual News News { get; set; }
    }
}
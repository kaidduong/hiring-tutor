﻿using HiringTutors.Core.Entities.EntityRelationships;
using HiringTutors.Core.Interfaces;
using HiringTutors.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.Entities
{
    public class Tutor : BaseEntity, IAggregateRoot, IAuditable
    {
        public string UserId { get; set; }
        public int? ScheduleId { get; set; }
        public string Job { get; set; }
        public int TeachingType { get; set; }
        public int TuitionPerHour { get; set; }
        public string Introduction { get; set; }
        public int Status { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<CertificationImage> CertificationImages { get; set; }

        public virtual ICollection<TutorSubject> TutorSubjects { get; set; }
        public virtual Schedule Schedule { get; set; }
        public virtual ICollection<TutorNews> TutorNews { get; set; }
    }
}
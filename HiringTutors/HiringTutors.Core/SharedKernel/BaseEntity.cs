﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HiringTutors.Core.SharedKernel
{
    public abstract class BaseEntity<TKey> where TKey : IEquatable<TKey>
    {
        public TKey Id { get; set; }
    }

    public abstract class BaseEntity : BaseEntity<int>
    {
    }
}